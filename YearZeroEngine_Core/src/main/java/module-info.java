/**
 * @author Stefan Prelle
 *
 */
module yearzeroengine.core {
	exports org.prelle.yearzeroengine.persist;
	exports org.prelle.yearzeroengine;
	exports org.prelle.yearzeroengine.modifications;
	exports org.prelle.yearzeroengine.charproc;

	opens org.prelle.yearzeroengine to simple.persist;
	opens org.prelle.yearzeroengine.modifications to simple.persist;

	requires java.xml;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires de.rpgframework.chars;
	requires de.rpgframework.products;
	requires simple.persist;
}