/**
 *
 */
package org.prelle.yearzeroengine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.modifications.ModificationChoice;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class YearZeroTools {

//	private final static ResourceBundle CORE = YearZeroEngineCore.getI18nResources();

	private final static Logger logger = LogManager.getLogger("yearzero");

	//--------------------------------------------------------------------
	public static void setModificationSource(YearZeroModule module, Object source) {
		if (module.getModifications()==null)
			return;
		for (Modification mod : module.getModifications()) {
			mod.setSource(source);
			if (mod instanceof ModificationChoice) {
				for (Modification mod2 : ((ModificationChoice)mod).getOptions()) {
					mod2.setSource(source);
				}
			}
		}
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @param aggregate Aggregate history elements with same adventure
//	 */
//	public static List<HistoryElementImpl> convertToHistoryElementList(YearZeroEngineCharacter charac, boolean aggregate) {
//		// Initial reward
//		logger.warn("Sort "+charac.getRewards().size()+" rewards  and "+charac.getHistory().size()+" mods");
//
//		/*
//		 * Build a merged list of rewards and modifications and sort it by time
//		 */
//		List<Datable> rewardsAndMods = new ArrayList<Datable>();
//		rewardsAndMods.addAll(charac.getRewards());
//		for (Modification mod : charac.getHistory()) {
//			rewardsAndMods.add(mod.clone());
//		}
////		rewardsAndMods.addAll(charac.getHistory());
//		sort(rewardsAndMods);
//
////		/*
////		 * Does the creation reward need to be added?
////		 */
////		if (rewardsAndMods.isEmpty() || !(rewardsAndMods.get(0) instanceof Reward)) {
////			RewardImpl creation = new RewardImpl(15, CORE.getString("label.reward.creation"));
////			rewardsAndMods.add(creation);
////			sort(rewardsAndMods);
////		}
//
//		/*
//		 * Now build a list of HistoryElements. Start a new H
//		 */
//		List<HistoryElementImpl> ret = new ArrayList<HistoryElementImpl>();
//		HistoryElementImpl current = null;
//		ProductService sessServ = RPGFrameworkLoader.getInstance().getProductService();
//
//		for (Datable item : rewardsAndMods) {
//			if (item instanceof RewardImpl) {
//				RewardImpl reward = (RewardImpl)item;
//				Adventure adv = null;
//				if (reward.getId()!=null) {
//					adv = sessServ.getAdventure(RoleplayingSystem.SHADOWRUN, reward.getId());
//					if (adv==null) {
//						logger.warn("Rewards of character '"+charac.getName()+"' reference an unknown adventure: "+reward.getId());
//					}
//				}
//				// If is same adventure as current, keep same history element
//				if (!aggregate || !(adv!=null && current!=null && adv==current.getAdventure()) ) {
//					current = new HistoryElementImpl();
//					current.setName(reward.getTitle());
//					if (adv!=null) {
//						current.setName(adv.getTitle());
//						current.setAdventure(adv);
//					}
//					ret.add(current);
//				}
//				current.addGained(reward);
//			} else if (item instanceof Modification) {
//				if (current==null) {
//					logger.error("Failed preparing history: Exp spent on modification without previous reward");
//				} else {
//					Modification lastMod = (current.getSpent().isEmpty())?null:current.getSpent().get(current.getSpent().size()-1);
//					if (lastMod!=null && lastMod.getClass()==item.getClass()) {
//						if (item instanceof SkillModification) {
////							logger.debug("Combine "+lastMod+" with "+item);
//							// Aggregate same skill
//							SkillModification lastSMod = (SkillModification)lastMod;
//							SkillModification newtMod = (SkillModification)item;
//							if (lastSMod.getSkill()==newtMod.getSkill()) {
//								// Same skill
//								lastSMod.setValue(newtMod.getValue());
//								lastSMod.setExpCost(lastSMod.getExpCost() + newtMod.getExpCost());
//							} else {
//								// Different skill
//								current.addSpent((Modification) item);
//							}
//						} else {
//							current.addSpent((Modification) item);
//						}
//					} else {
//						current.addSpent((Modification) item);
//					}
//				}
//			} else if (item instanceof CurrencyModification) {
//				if (current==null) {
//					logger.error("Failed preparing history: Exp spent on modification without previous reward");
//					current = new HistoryElementImpl();
//					current.setName("???");
//					current.addSpent((Modification) item);
//					ret.add(current);
//				} else {
//				}
//			} else {
//				logger.error("Don't know how to "+item);
//			}
//		}
//
//
//		logger.warn("  return "+ret.size()+" elements");
//		return ret;
//	}

//	//-------------------------------------------------------------------
//	private static void sort(List<Datable> rewardsAndMods) {
//		Collections.sort(rewardsAndMods, new Comparator<Datable>() {
//			public int compare(Datable o1, Datable o2) {
//				Long time1 = 0L;
//				Long time2 = 0L;
//				if (o1==null || o2==null) {
//					logger.error("Trying to compare "+o1+" with "+o2+" when sorting");
//					return 0;
//				}
//				if (o1.getDate()!=null)	time1 = o1.getDate().getTime();
//				if (o2.getDate()!=null)	time2 = o2.getDate().getTime();
//
//				int cmp = time1.compareTo(time2);
//				if (cmp==0) {
//					if (o1 instanceof RewardImpl && o2 instanceof Modification) return -1;
//					if (o1 instanceof Modification && o2 instanceof RewardImpl) return  1;
//				}
//				return cmp;
//			}
//		});
//	}

}
