/**
 *
 */
package org.prelle.yearzeroengine;

import java.util.MissingResourceException;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.yearzeroengine.persist.AttributeConverter;
import org.prelle.yearzeroengine.persist.MappingSkillCategoryConverter;

/**
 * @author prelle
 *
 */
public class Skill extends YearZeroModule {

	@org.prelle.simplepersist.Attribute
	@AttribConvert(AttributeConverter.class)
	private Attribute attr;
	@org.prelle.simplepersist.Attribute(name="cat")
	@AttribConvert(MappingSkillCategoryConverter.class)
	private SkillCategory category;

	//-------------------------------------------------------------------
	/**
	 */
	public Skill() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		if (i18n==null)
			return id;
		String key = "skill."+id;
		try {
			return i18n.getString(key);
		} catch (MissingResourceException mre) {
			logger.error("Missing property '"+key+"' in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(mre.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "skill."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "skill."+id+".desc";
	}

	//-------------------------------------------------------------------
	public String toString() {
		return id+" ("+attr+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the attr
	 */
	public Attribute getAttribute() {
		return attr;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the category
	 */
	public SkillCategory getCategory() {
		return category;
	}

	//-------------------------------------------------------------------
	/**
	 * @param category the category to set
	 */
	public void setCategory(SkillCategory category) {
		this.category = category;
	}

}
