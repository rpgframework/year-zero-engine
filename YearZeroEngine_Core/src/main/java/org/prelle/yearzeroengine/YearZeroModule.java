/**
 * 
 */
package org.prelle.yearzeroengine;

import java.text.Collator;

import org.prelle.simplepersist.Element;
import org.prelle.yearzeroengine.modifications.ModificationList;

/**
 * @author prelle
 *
 */
public abstract class YearZeroModule extends BasePluginData implements Comparable<YearZeroModule> {
	
	@Element
	protected ModificationList modifications;

	//-------------------------------------------------------------------
	public YearZeroModule() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public abstract String getName();

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public ModificationList getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(YearZeroModule o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

}
