/**
 * 
 */
package org.prelle.yearzeroengine.charproc;

import java.util.List;

import org.prelle.yearzeroengine.YearZeroEngineCharacter;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public interface CharacterProcessor {

	/**
	 * @param unprocessed Unprocessed modifications from previous steps
	 * @return Unprocessed modification
	 */
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> unprocessed);

	public List<ToDoElement> getToDos();
	
}
