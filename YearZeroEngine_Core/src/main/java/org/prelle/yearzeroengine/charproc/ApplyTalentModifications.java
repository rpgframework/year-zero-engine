/**
 *
 */
package org.prelle.yearzeroengine.charproc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.modifications.AttributeModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ApplyTalentModifications implements CharacterProcessor {

	private static Logger logger = LogManager.getLogger("yearzero");

	//--------------------------------------------------------------------
	public ApplyTalentModifications() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model,
			List<Modification> previous) {
		for (TalentValue val : model.getTalents()) {
			if (!val.getTalent().getModifications().isEmpty()) {
				logger.debug("Inject modifications from talent "+val.getTalent());
				for (Modification tmp : val.getTalent().getModifications()) {
					if (tmp instanceof AttributeModification) {
						AttributeModification mod = (AttributeModification)tmp;
						model.getAttribute(mod.getAttribute()).addModification(mod);
						logger.info("Applied "+tmp+" from "+mod.getSource());
					} else
						logger.warn("Did not know how to apply "+tmp);
				}
			}
		}
		return previous;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return new ArrayList<ToDoElement>();
	}

}
