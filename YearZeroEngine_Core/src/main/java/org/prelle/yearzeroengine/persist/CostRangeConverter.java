/**
 *
 */
package org.prelle.yearzeroengine.persist;

import java.util.StringTokenizer;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.yearzeroengine.CostRange;

/**
 * @author Stefan
 *
 */
public class CostRangeConverter implements StringValueConverter<CostRange> {

	//--------------------------------------------------------------------
	/**
	 */
	public CostRangeConverter() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(CostRange value) throws Exception {
		if (value.isFlexibel()) {
			return value.getMin()+"-"+value.getMax();
		}
		return String.valueOf(value.getMin());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public CostRange read(String v) throws Exception {
		StringTokenizer tok = new StringTokenizer(v,"-");
		int min = Integer.parseInt(tok.nextToken());
		int max = 0;
		if (tok.hasMoreTokens())
			max = Integer.parseInt(tok.nextToken());
		return new CostRange(min,max);
	}

}
