package org.prelle.yearzeroengine.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.yearzeroengine.ItemTemplate;
import org.prelle.yearzeroengine.YearZeroEngineCore;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;

import de.rpgframework.core.RoleplayingSystem;

@ConstructorParams({YearZeroEngineCore.KEY_RULES, YearZeroEngineCore.ITEM_TEMPLATE_CLASS})
public class ItemConverter implements StringValueConverter<ItemTemplate> {

	private final static Logger logger = LogManager.getLogger("yze.persist");

	private YearZeroEngineRuleset ruleset;
	private Class<? extends ItemTemplate> itemTemplateClass;

	//-------------------------------------------------------------------
	public ItemConverter(RoleplayingSystem rules, Class<? extends ItemTemplate> cls) {
		ruleset = YearZeroEngineCore.getRuleset(rules);
		itemTemplateClass = cls;
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in YearZeroEngineCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public ItemTemplate read(String v) throws Exception {
		ItemTemplate item = ruleset.getByType(itemTemplateClass, v);
//		if (item==null) {
//			item = SplittermondCustomDataCore.getItem(v);
//		}
		if (item==null) {
			logger.error("Unknown item reference: '"+v+"'");
			throw new SerializationException("Unknown item template "+v);
		}
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(ItemTemplate v) throws Exception {
		return v.getId();
	}

}