/**
 * 
 */
package org.prelle.yearzeroengine.persist;

import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.yearzeroengine.SkillCategory;
import org.prelle.yearzeroengine.YearZeroEngineCore;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({YearZeroEngineCore.KEY_RULES})
public class MappingSkillCategoryConverter implements StringValueConverter<SkillCategory> {

	private YearZeroEngineRuleset ruleset;
	
	//-------------------------------------------------------------------
	public MappingSkillCategoryConverter(RoleplayingSystem rules) {
		ruleset = YearZeroEngineCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in YearZeroEngine");
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(SkillCategory value) throws Exception {
		return ruleset.getSkillCategoryConverter().write(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public SkillCategory read(String idref) throws Exception {
		return ruleset.getSkillCategoryConverter().read(idref);
	}

}
