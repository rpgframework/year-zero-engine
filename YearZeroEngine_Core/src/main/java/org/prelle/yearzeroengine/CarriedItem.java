/**
 *
 */
package org.prelle.yearzeroengine;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.yearzeroengine.modifications.ModificationList;
import org.prelle.yearzeroengine.persist.ItemConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class CarriedItem<T extends ItemTemplate> extends UniqueObject implements Comparable<CarriedItem<T>> {

	@org.prelle.simplepersist.Attribute(name="ref",required=true)
	@AttribConvert(ItemConverter.class)
	protected T ref;
	@org.prelle.simplepersist.Attribute(name="count",required=false)
	protected int count;
	@Attribute
	protected String name;
	@Element
	protected ModificationList modifications;

	//--------------------------------------------------------------------
	public CarriedItem() {
		modifications = new ModificationList();
	}

	//--------------------------------------------------------------------
	public CarriedItem(T item) {
		this.ref = item;
		modifications = new ModificationList();
	}

	//--------------------------------------------------------------------
	public String getName() {
		if (name!=null)
			return name;
		return ref.getName();
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CarriedItem<T> o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//--------------------------------------------------------------------
	public String toString() {
		return "CarriedItem "+ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the item
	 */
	public T getItem() {
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @param item the item to set
	 */
	public void setItem(T item) {
		this.ref = item;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	//--------------------------------------------------------------------
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	//--------------------------------------------------------------------
	public int getBonus() {
		return ref.getBonus();
	}

	//--------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		return new ArrayList<Modification>(modifications);
	}

}
