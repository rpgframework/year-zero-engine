/**
 *
 */
package org.prelle.yearzeroengine;

/**
 * @author Stefan
 *
 */
public class CostRange {

	private int min;
	private int max;

	//--------------------------------------------------------------------
	public CostRange(int min, int max) {
		this.min = min;
		this.max = max;
	}

	//--------------------------------------------------------------------
	public boolean isFlexibel() {
		return min<max;
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (max==0)
			return String.valueOf(min);
		return min+"-"+max;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the min
	 */
	public int getMin() {
		return min;
	}

	//--------------------------------------------------------------------
	/**
	 * @param min the min to set
	 */
	public void setMin(int min) {
		this.min = min;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	//--------------------------------------------------------------------
	/**
	 * @param max the max to set
	 */
	public void setMax(int max) {
		this.max = max;
	}

}
