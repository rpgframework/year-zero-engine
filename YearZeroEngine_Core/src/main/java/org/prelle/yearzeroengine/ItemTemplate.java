/**
 *
 */
package org.prelle.yearzeroengine;

import java.util.MissingResourceException;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.yearzeroengine.persist.CostRangeConverter;

/**
 * @author prelle
 *
 */
public class ItemTemplate extends YearZeroModule {

	@Attribute
	@AttribConvert(CostRangeConverter.class)
	protected CostRange cost;
	@Attribute
	protected int bonus;

	//-------------------------------------------------------------------
	public ItemTemplate() {
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Item:"+id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.YearZeroModule#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return id;
		try {
			return i18n.getString("item."+getId());
		} catch (MissingResourceException e) {
			if (!reportedKeys.contains(e.getKey())) {
				reportedKeys.add(e.getKey());

				logger.error("Missing property "+e.getKey()+" in "+i18n.getBaseBundleName());
				if (MISSING!=null)
					MISSING.println(e.getKey()+"=");
			}
		}
		return "item."+getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "item."+getId()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "item."+getId()+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public CostRange getCost() {
		return cost;
	}

	//-------------------------------------------------------------------
	/**
	 * @param cost the cost to set
	 */
	public void setCost(CostRange cost) {
		this.cost = cost;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the bonus
	 */
	public int getBonus() {
		return bonus;
	}

	//-------------------------------------------------------------------
	/**
	 * @param bonus the bonus to set
	 */
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

}
