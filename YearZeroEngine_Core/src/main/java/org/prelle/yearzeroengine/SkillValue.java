/**
 * 
 */
package org.prelle.yearzeroengine;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.persist.SkillConverter;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name="skillval")
public class SkillValue extends ModifyableImpl implements Comparable<SkillValue>, ModifyableValue<Skill> {

	@org.prelle.simplepersist.Attribute(name="id",required=true)
	@AttribConvert(SkillConverter.class)
	private Skill id;

	/**
	 * The recent value of the attribute, including modifications by
	 * race on generation, the distributed points on generation
	 * and the points bought.
	 * Bought = unmodifiedValue - start;
	 */
	@org.prelle.simplepersist.Attribute(name="value",required=true)
	private int distributed;

	/**
	 * The final value of the attribute after generation, before
	 * exp have been spent.
	 * During priority generation this contains the value until
	 * which the attribute is paid by attribute points
	 */
	@org.prelle.simplepersist.Attribute(name="start",required=false)
	private int start;


	//-------------------------------------------------------------------
	public SkillValue() {
	}

	//-------------------------------------------------------------------
	public SkillValue(Skill attr, int val) {
		this.id = attr;
		this.distributed = val;
		this.start = val;
	}

	//-------------------------------------------------------------------
	public SkillValue(SkillValue copy) {
		this.id = copy.getModifyable();
		this.start = copy.getStart();
		this.distributed = copy.getPoints();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%s  \tSTART=%d  NORM=%d MOD=%d  VAL=%d", id, start, distributed, getModifier(), getModifiedValue());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public Skill getSkill() {
		return id;
	}

	//-------------------------------------------------------------------
	public int getModifier() {
//		int count = 0;
//		for (Modification mod : modifications) {
////			if (mod instanceof SkillModification) {
////				SkillModification aMod = (SkillModification)mod;
////				if (aMod.getSkill()==id && aMod.getType()==ModificationValueType.CURRENT)
////					count += aMod.getValue();
////			}
//		}
		return 0; //count;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	@Override
	public int getModifiedValue() {
		return distributed + getModifier();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public Skill getModifyable() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SkillValue other) {
		try {
			return id.getName().compareTo(other.getModifyable().getName());
		} catch (Exception e) {
			System.out.println("id="+id);
			System.out.println("id.getName()="+id.getName());
			System.out.println("other="+other);
			System.out.println("other.getModifyable()="+other.getModifyable());
			System.out.println("other.getModifyable().getName()="+other.getModifyable().getName());
			System.exit(0);
			return 0;
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int distributed) {
		this.distributed = distributed;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the start
	 */
	public int getStart() {
		return start;
	}

	//-------------------------------------------------------------------
	/**
	 * @param start the start to set
	 */
	public void setStart(int start) {
		this.start = start;
	}

	//-------------------------------------------------------------------
	/**
	 * Points gained by investing EXP
	 * @return the v1Value
	 */
	public int getBought() {
		return distributed - start;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return distributed;
	}

}
