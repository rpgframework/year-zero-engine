/**
 * 
 */
package org.prelle.yearzeroengine;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="classes")
@ElementList(entry="class",type=CharacterClass.class,inline=true)
public class CharacterClassList extends ArrayList<CharacterClass> {

}
