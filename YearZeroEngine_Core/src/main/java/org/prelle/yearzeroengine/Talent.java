/**
 *
 */
package org.prelle.yearzeroengine;

import java.util.MissingResourceException;

/**
 * @author prelle
 *
 */
public class Talent extends YearZeroModule implements Comparable<YearZeroModule> {

	public enum Type {
		GROUP,
		GENERAL,
		ICON,
		HUMANITE,
		CYBERNETIC,
		BIONIC,
		MYSTICAL,
	}

	@org.prelle.simplepersist.Attribute(required=true)
	private Type type;

	//-------------------------------------------------------------------
	/**
	 */
	public Talent() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		if (i18n==null)
			return id;
		String key = "talent."+id;
		try {
			return i18n.getString(key);
		} catch (MissingResourceException mre) {
			logger.error("Missing property '"+key+"' in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(mre.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "talent."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "talent."+id+".desc";
	}

	//-------------------------------------------------------------------
	public String toString() {
		return id+" ("+type+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

}
