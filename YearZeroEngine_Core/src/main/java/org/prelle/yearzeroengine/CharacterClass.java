/**
 *
 */
package org.prelle.yearzeroengine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;

/**
 * @author prelle
 *
 */
public class CharacterClass extends YearZeroModule {

	private Map<String, List<String>> fluffOptions;

	//-------------------------------------------------------------------
	public CharacterClass() {
		fluffOptions = new HashMap<>();
	}

	//-------------------------------------------------------------------
	public String toString() { return id; }

	//-------------------------------------------------------------------
	public String getName() {
		if (i18n==null)
			return id;
		String key = "characterclass."+id;
		try {
			return i18n.getString(key);
		} catch (MissingResourceException mre) {
			logger.error("Missing property '"+key+"' in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(mre.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "characterclass."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "characterclass."+id+".desc";
	}

	//-------------------------------------------------------------------
	public void setFluffOptions(String key, List<String> optionTexts) {
		fluffOptions.put(key, optionTexts);
	}

	//-------------------------------------------------------------------
	public List<String> getFluffOptions(String key) {
		return new ArrayList<String>(fluffOptions.get(key));
	}

}
