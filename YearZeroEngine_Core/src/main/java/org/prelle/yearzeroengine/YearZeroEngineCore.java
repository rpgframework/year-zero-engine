/**
 *
 */
package org.prelle.yearzeroengine;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.rpgframework.character.RulePlugin;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class YearZeroEngineCore {

	protected static Logger logger = LogManager.getLogger("yearzero");

	public final static String KEY_RULES = "rulesCoriolis";
	public final static String ITEM_TEMPLATE_CLASS = "itemTemplateYZE";

	private static PropertyResourceBundle i18NResources;

	private static Map<RoleplayingSystem, YearZeroEngineRuleset> rulesets;

	//-------------------------------------------------------------------
	/**
	 */
	public YearZeroEngineCore() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	static {
		i18NResources = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/yearzeroengine/i18n/yearzeroengine_core");
		rulesets = new HashMap<RoleplayingSystem, YearZeroEngineRuleset>();
	}

	//-------------------------------------------------------------------
	public static PropertyResourceBundle getI18nResources() {
		return i18NResources;
	}

	//-------------------------------------------------------------------
	public static YearZeroEngineRuleset getRuleset(RoleplayingSystem rules) {
		return rulesets.get(rules);
	}

	//-------------------------------------------------------------------
	public static YearZeroEngineRuleset createRuleset(RoleplayingSystem rules, PropertyResourceBundle i18n, PropertyResourceBundle i18nHelp) {
		if (rulesets.containsKey(rules))
			return rulesets.get(rules);
//			throw new IllegalStateException("Already have a ruleset for "+rules);

		YearZeroEngineRuleset ruleset = new YearZeroEngineRuleset(rules, i18n, i18nHelp);
		rulesets.put(rules, ruleset);

		return ruleset;
	}

	//-------------------------------------------------------------------
	public static CharacterClassList loadCharacterClasses(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load character classes (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		CharacterClassList list = null;
		try {
			list = ruleset.getSerializer().read(CharacterClassList.class, in);
			logger.info("Successfully loaded "+list.size()+" character classes");

			// Set translation
			for (CharacterClass tmp : list) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
				ruleset.addCharacterClass(tmp);

				// Fix modifications
				YearZeroTools.setModificationSource(tmp, tmp);

			}

			if (logger.isDebugEnabled()) {
				for (CharacterClass tmp : list)
					logger.debug("* "+tmp.getName());
			}

		} catch (Exception e) {
			logger.fatal("Failed deserializing templates",e);
			System.exit(0);
		}
		return list;
	}

	//-------------------------------------------------------------------
	public static void loadSkills(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load skills (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		assert resources!=null;
		try {
			SkillList skills = ruleset.getSerializer().read(SkillList.class, in);
			logger.info("Successfully loaded "+skills.size()+" skills");

			// Set translation
			for (Skill tmp : new ArrayList<>(skills)) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
			}

			Collections.sort(skills);

			if (logger.isDebugEnabled()) {
				for (Skill tmp : skills)
					logger.debug("* "+tmp.getName());
			}

			ruleset.addSkills(skills);
		} catch (Exception e) {
			logger.fatal("Failed deserializing skills",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void loadTalents(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load talents (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		assert resources!=null;
		try {
			TalentList talents = ruleset.getSerializer().read(TalentList.class, in);
			logger.info("Successfully loaded "+talents.size()+" talents");

			// Set translation
			for (Talent tmp : new ArrayList<>(talents)) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();

				// Fix modifications
				YearZeroTools.setModificationSource(tmp, tmp);
			}

			Collections.sort(talents);

			if (logger.isDebugEnabled()) {
				for (Talent tmp : talents)
					logger.debug("* "+tmp.getName());
			}

			ruleset.addByType(Talent.class, talents);
		} catch (Exception e) {
			logger.fatal("Failed deserializing talents",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static <I extends ItemTemplate> void loadItems(YearZeroEngineRuleset ruleset, RulePlugin<? extends YearZeroEngineCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources, Class<I> itemClass, Class<? extends ArrayList<I>> listClass) {
		logger.debug("Load items (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		assert resources!=null;
		try {
			ArrayList<I> toAdd = ruleset.getSerializer().read(listClass, in);
			logger.info("Successfully loaded "+toAdd.size()+" items");

			// Set translation
			for (ItemTemplate tmp : new ArrayList<>(toAdd)) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();

				// Fix modifications
				YearZeroTools.setModificationSource(tmp, tmp);
			}

			Collections.sort(toAdd);

			if (logger.isDebugEnabled()) {
				for (ItemTemplate tmp : toAdd)
					logger.debug("* "+tmp.getName());
			}

			ruleset.addByType(itemClass, toAdd);
		} catch (Exception e) {
			logger.fatal("Failed deserializing items",e);
			return;
		}
	}

}
