/**
 *
 */
package org.prelle.yearzeroengine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="attributes")
@ElementList(entry="attr",type=AttributeValue.class)
public class Attributes extends ArrayList<AttributeValue> {

	private static final long serialVersionUID = 1L;

	private static Logger logger = LogManager.getLogger("yearzero");

	private transient Map<Attribute, AttributeValue> secondary;

	//-------------------------------------------------------------------
	public Attributes() {
		secondary = new HashMap<Attribute, AttributeValue>();
	}

	//--------------------------------------------------------------------
	public void addSecondary(AttributeValue pair) {
		secondary.put(pair.getAttribute(), pair);
	}

//	//-------------------------------------------------------------------
//	public boolean add(AttributeValue pair) {
//		if (pair.getAttribute().isPrimary() || pair.getAttribute().isSpecial()) {
//			for (AttributeValue check : this) {
//				if (check.getAttribute()==pair.getAttribute()) {
//					check.setPoints(pair.getPoints());
//					check.setStart(pair.getStart());
////					check.setGenerationModifier(pair.getGenerationModifier());
////					check.setUnmodifiedValue(pair.getUnmodifiedValue());
//					return true;
//				}
//			}
//			super.add(pair);
//		} else
//			secondary.put(pair.getAttribute(), pair);
//
//		return true;
//	}
//

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		for (AttributeValue tmp : this)
			buf.append("\n "+tmp.getAttribute()+" \t "+tmp);
//		for (Attribute tmp : Attribute.secondaryValues())
//			buf.append("\n "+tmp+" \t "+get(tmp));

		return buf.toString();
	}

	//-------------------------------------------------------------------
	public int getValue(Attribute key) {
		return get(key).getModifiedValue();
	}

	//-------------------------------------------------------------------
	public AttributeValue get(Attribute key) {
		if (key==null)
			throw new NullPointerException("Attribute may not be null");
		for (AttributeValue pair : this) {
			if (pair.getAttribute()==key) {
				return pair;
			}
		}
		AttributeValue pair = secondary.get(key);
		if (pair!=null) {
			return pair;
		}

		logger.error("Something accessed an unset attribute: "+key+"\nsecondary="+secondary);
		logger.error(dump());
		throw new NoSuchElementException(String.valueOf(key));
	}

//	//--------------------------------------------------------------------
//	public void addModification(AttributeModification mod) {
//		get(mod.getAttribute()).addModification(mod);
//	}
//
//	//--------------------------------------------------------------------
//	public void removeModification(AttributeModification mod) {
//		get(mod.getAttribute()).removeModification(mod);
//	}

}
