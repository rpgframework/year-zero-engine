/**
 *
 */
package org.prelle.yearzeroengine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class YearZeroEngineRuleset {

	private Logger logger;

	private PropertyResourceBundle i18N;
	private PropertyResourceBundle i18nHelp;

	private RoleplayingSystem ruleset;

	private Persister serializer;
	private Map<String,Attribute> attributes;
//	private GenerationTemplateList templates;
//	private MotivationList motivations;
	private Map<String,CharacterClass> classes;
	private SkillList skills;
//	private TalentList talents;
	private Map<Class<? extends YearZeroModule>, List<? extends YearZeroModule>> listsByType;
	private StringValueConverter<? extends SkillCategory> skillCategoryConverter;

	//-------------------------------------------------------------------
	public YearZeroEngineRuleset(RoleplayingSystem rules, PropertyResourceBundle i18n, PropertyResourceBundle i18nHelp) {
		this.ruleset = rules;
		this.i18N = i18n;
		this.i18nHelp = i18nHelp;
		logger = LogManager.getLogger("yearzeroengine."+rules.name().toLowerCase());

		serializer = new Persister();
		Persister.putContext(YearZeroEngineCore.KEY_RULES, rules);

		attributes  = new HashMap<>();
//		templates   = new GenerationTemplateList();
		skills      = new SkillList();
//		motivations = new MotivationList();
		classes     = new HashMap<>();
//		talents     = new TalentList();
//		flaws	    = new FlawList();
//		items       = new ItemTemplateList();
		listsByType = new HashMap<>();
//		utility = new UbiquityUtils();
	}

	//-------------------------------------------------------------------
	public RoleplayingSystem getRoleplayingSystem() {
		return ruleset;
	}

	//-------------------------------------------------------------------
	public PropertyResourceBundle getI18nProperties() {
		return i18N;
	}

	//-------------------------------------------------------------------
	public Serializer getSerializer() {
		return serializer;
	}

	//-------------------------------------------------------------------
	public void addSkills(Collection<Skill> toAdd) {
		logger.debug("Add "+toAdd.size()+" skills to "+ruleset+" ruleset");
		skills.addAll(toAdd);
		Collections.sort(skills);
	}

	//-------------------------------------------------------------------
	public List<Skill> getSkills() {
		return new ArrayList<Skill>(skills);
	}

	//-------------------------------------------------------------------
	public List<Skill> getSkillsByCategory(SkillCategory cat) {
		return getSkills().stream().filter( (t) -> t.getCategory()==cat).collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	public Skill getSkill(String key) {
		if (key==null) throw new NullPointerException();
		for (Skill tmp : skills) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addAttribute(Attribute attr) {
		attributes.put(attr.name(), attr);
	}

	//-------------------------------------------------------------------
	public Attribute getAttribute(String name) {
		return attributes.get(name);
	}

	//-------------------------------------------------------------------
	public List<Attribute> getAttributes() {
		List<Attribute> ret = new ArrayList<>(attributes.values());
		Collections.sort(ret, (o1, o2) -> o1.getName().compareTo(o2.getName() ));
		return ret;
	}

	//-------------------------------------------------------------------
	public void addCharacterClass(CharacterClass attr) {
		classes.put(attr.getId(), attr);
	}

	//-------------------------------------------------------------------
	public CharacterClass getCharacterClass(String name) {
		return classes.get(name);
	}

	//-------------------------------------------------------------------
	public List<CharacterClass> getCharacterClasses() {
		List<CharacterClass> ret = new ArrayList<>(classes.values());
		Collections.sort(ret, (o1, o2) -> o1.getName().compareTo(o2.getName() ));
		return ret;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public <T extends YearZeroModule> List<T> getListByType(Class<T> cls) {
		if (listsByType.get(cls)==null) {
			throw new NoSuchElementException("No lists of type "+cls+" in ruleset "+ruleset.hashCode()+"\nOnly have "+listsByType.keySet());
		}
		List<T> ret = new ArrayList<T>( (List<T>) listsByType.get(cls) );
		if (Comparable.class.isAssignableFrom(cls)) {
			Collections.sort(ret);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public <T extends YearZeroModule> void addByType(T data) {
		List<T> list = (List<T>) listsByType.get(data.getClass());
		if (list==null) {
			list = new ArrayList<>();
			listsByType.put(data.getClass(), list);
		}
		list.add(data);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public <T extends YearZeroModule> void addByType(Class<T> cls, Collection<T> toAdd) {
		List<T> list = (List<T>) listsByType.get(cls);
		if (list==null) {
			list = new ArrayList<>();
			listsByType.put(cls, list);
		}
		list.addAll(toAdd);
		Collections.sort(list);
	}

	//-------------------------------------------------------------------
	public <T extends YearZeroModule> T getByType(Class<T> cls, String id) {
		for (T tmp : getListByType(cls)) {
			if (tmp.getId().equals(id))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skillCategoryConverter
	 */
	@SuppressWarnings("unchecked")
	public StringValueConverter<SkillCategory> getSkillCategoryConverter() {
		return (StringValueConverter<SkillCategory>) skillCategoryConverter;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skillCategoryConverter the skillCategoryConverter to set
	 */
	public void setSkillCategoryConverter(StringValueConverter<? extends SkillCategory> skillCategoryConverter) {
		this.skillCategoryConverter = skillCategoryConverter;
	}

}

