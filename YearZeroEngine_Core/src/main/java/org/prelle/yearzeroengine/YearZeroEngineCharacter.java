/**
 *
 */
package org.prelle.yearzeroengine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.yearzeroengine.modifications.ModificationList;
import org.prelle.yearzeroengine.persist.CharacterClassConverter;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public abstract class YearZeroEngineCharacter implements RuleSpecificCharacterObject {

//	private final static Logger logger = Logger.getLogger("yearzeroengine");
	private static PropertyResourceBundle res = YearZeroEngineCore.getI18nResources();

	public enum Gender {
		MALE,
		FEMALE
		;
		public String toString() {
			return res.getString("gender."+name().toLowerCase());
		}
	}

	private YearZeroEngineRuleset ruleset;

	@Element
	private String name;
	@ElementList(entry="attribute",type=AttributeValue.class)
	protected Attributes attributes;
	@Element(required=false)
	private byte[] image;
	@org.prelle.simplepersist.Attribute(name="class")
	@AttribConvert(CharacterClassConverter.class)
	private CharacterClass characterClass;
	@ElementList(entry="skillval", type=SkillValue.class)
	private List<SkillValue> skillvals;
	@ElementList(entry="talentval", type=TalentValue.class)
	private List<TalentValue> talentvals;
	@Element
	private String problem;
	@ElementList(entry="relation",type=CharacterRelation.class,required=true)
	protected List<CharacterRelation> relations;
	@Element
	protected String face;
	@Element
	protected String clothing;
	@org.prelle.simplepersist.Attribute(name="epFree")
	protected int expFree;
	@org.prelle.simplepersist.Attribute(name="epInv")
	protected int expInv;

	@Element
	protected ModificationList history;
	@Element
	protected RewardList rewards;


	//-------------------------------------------------------------------
	public YearZeroEngineCharacter() {
		attributes = new Attributes();
		skillvals  = new ArrayList<>();
		talentvals = new ArrayList<>();
		relations  = new ArrayList<>();
		history = new ModificationList();
		rewards = new RewardList();
	}

	//-------------------------------------------------------------------
	public abstract void initAttributes(Attribute[] values);
//	public void initAttributes(Attribute[] values) {
//		for (Attribute attr : values)
//			attributes.add(new AttributeValue(attr, 0));
//	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof YearZeroEngineCharacter) {
			YearZeroEngineCharacter other = (YearZeroEngineCharacter)o;
			if (!name.equals(other.getName())) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return "YearZeroEngineChar "+name;
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getName()
	 */
	@Override
	public String getName() {
		return name;
	}


	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getImage()
	 */
	@Override
	public byte[] getImage() {
		return image;
	}

	//-------------------------------------------------------------------
	/**
	 * @param image the image to set
	 */
	public void setImage(byte[] image) {
		this.image = image;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the ruleset
	 */
	public YearZeroEngineRuleset getRuleset() {
		return ruleset;
	}

	//--------------------------------------------------------------------
	/**
	 * @param ruleset the ruleset to set
	 */
	public void setRuleset(YearZeroEngineRuleset ruleset) {
		assert ruleset!=null;
		this.ruleset = ruleset;
	}

	//-------------------------------------------------------------------
	public AttributeValue getAttribute(Attribute key) {
		return attributes.get(key);
	}

	//-------------------------------------------------------------------
	public String dumpAttributes() {
		StringBuffer buf = new StringBuffer();
		for (Attribute key : ruleset.getAttributes()) {
		  buf.append(key+" = "+attributes.get(key)+"\n");
		}
		for (SkillValue key : getSkills()) {
			  buf.append(key);
			}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the characterClass
	 */
	public CharacterClass getCharacterClass() {
		return characterClass;
	}

	//-------------------------------------------------------------------
	/**
	 * @param characterClass the characterClass to set
	 */
	public void setCharacterClass(CharacterClass characterClass) {
		this.characterClass = characterClass;
	}

//	//-------------------------------------------------------------------
//	public Collection<AttributeValue> getAttributes(Attribute[] select) {
//		List<AttributeValue> ret = new ArrayList<>();
//		for (Attribute attr : select)
//			ret.add(attributes.get(attr));
//		return ret;
//	}

	//-------------------------------------------------------------------
	public List<SkillValue> getSkills() {
		List<SkillValue> ret = new ArrayList<SkillValue>(skillvals);
		// Sort alphabetically
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<SkillValue> getSkills(List<Skill> requested) {
		List<SkillValue> ret = new ArrayList<SkillValue>();
		for (SkillValue val : getSkills()) {
			if (requested.contains(val.getModifyable()))
				ret.add(val);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public SkillValue getSkillValue(Skill skill) {
		if (skill==null)
			throw new NullPointerException();
		for (SkillValue tmp : skillvals)
			if (tmp.getSkill()==skill)
				return tmp;

		SkillValue sVal = new SkillValue(skill, 0);
		skillvals.add(sVal);
		return sVal;
	}

	//-------------------------------------------------------------------
	public void addTalent(TalentValue value) {
		if (getTalentValue(value.getTalent())!=null)
			throw new IllegalStateException("Already exists");
		talentvals.add(value);
	}

	//-------------------------------------------------------------------
	public void removeTalent(TalentValue value) {
		talentvals.remove(value);
	}

	//-------------------------------------------------------------------
	public List<TalentValue> getTalents() {
		List<TalentValue> ret = new ArrayList<TalentValue>(talentvals);
		// Sort alphabetically
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public TalentValue getTalentValue(Talent key) {
		for (TalentValue tmp : talentvals)
			if (tmp.getTalent()==key)
				return tmp;

		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the relations
	 */
	public List<CharacterRelation> getRelations() {
		return relations;
	}

	//-------------------------------------------------------------------
	public void setRelations(List<CharacterRelation> relations) {
		this.relations = relations;
	}

	//-------------------------------------------------------------------
	public void addRelations(CharacterRelation value) {
		this.relations.add(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the face
	 */
	public String getFace() {
		return face;
	}

	//-------------------------------------------------------------------
	/**
	 * @param face the face to set
	 */
	public void setFace(String face) {
		this.face = face;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the clothing
	 */
	public String getClothing() {
		return clothing;
	}

	//-------------------------------------------------------------------
	/**
	 * @param clothing the clothing to set
	 */
	public void setClothing(String clothing) {
		this.clothing = clothing;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the problem
	 */
	public String getProblem() {
		return problem;
	}

	//--------------------------------------------------------------------
	/**
	 * @param problem the problem to set
	 */
	public void setProblem(String problem) {
		this.problem = problem;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the expFree
	 */
	public int getExpFree() {
		return expFree;
	}

	//--------------------------------------------------------------------
	/**
	 * @param expFree the expFree to set
	 */
	public void setExpFree(int expFree) {
		this.expFree = expFree;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the expInv
	 */
	public int getExpInvested() {
		return expInv;
	}

	//--------------------------------------------------------------------
	/**
	 * @param expInv the expInv to set
	 */
	public void setExpInvested(int expInv) {
		this.expInv = expInv;
	}

	//--------------------------------------------------------------------
	public void addToHistory(Modification mod) {
		if (mod.getDate()==null)
			mod.setDate(new Date(System.currentTimeMillis()));
		history.add(mod);
	}

	//--------------------------------------------------------------------
	public void removeFromHistory(Modification mod) {
		history.remove(mod);
	}

	//--------------------------------------------------------------------
	public List<Modification> getHistory() {
		return new ArrayList<Modification>(history);
	}

	//--------------------------------------------------------------------
	public void addReward(Reward rew) {
		if (rew.getDate()==null)
			rew.setDate(new Date(System.currentTimeMillis()));
		if (!rewards.contains(rew))
			rewards.add(rew);
	}

	//--------------------------------------------------------------------
	public List<Reward> getRewards() {
		return new ArrayList<Reward>(rewards);
	}

}
