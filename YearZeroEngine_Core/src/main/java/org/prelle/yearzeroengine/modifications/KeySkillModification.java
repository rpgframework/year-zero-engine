package org.prelle.yearzeroengine.modifications;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.yearzeroengine.Skill;
import org.prelle.yearzeroengine.persist.SkillConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KeySkillModification extends ModificationImpl {

	@org.prelle.simplepersist.Attribute(name="ref")
	@AttribConvert(SkillConverter.class)
	private Skill skill;
    
    //-----------------------------------------------------------------------
    public KeySkillModification() {
     }
    
    //-----------------------------------------------------------------------
    public KeySkillModification(Skill skill) {
        this.skill = skill;
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
    	if (skill==null)
    		return "Unknown skill";
        return skill.getName();
    }
    //-----------------------------------------------------------------------
    public Skill getSkill() {
        return skill;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof KeySkillModification) {
            KeySkillModification amod = (KeySkillModification)o;
            if (amod.getSkill()!=skill) return false;
            return true;
        } else
            return false;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof KeySkillModification) {
            KeySkillModification amod = (KeySkillModification)o;
            if (amod.getSkill()!=skill) return false;
            return true;
        } else
            return false;
    }
    
    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof KeySkillModification))
            return toString().compareTo(obj.toString());
        KeySkillModification other = (KeySkillModification)obj;
        if (skill!=other.getSkill())
            return skill.getName().compareTo(other.getSkill().getName());
        return 0;
    }
    
}
