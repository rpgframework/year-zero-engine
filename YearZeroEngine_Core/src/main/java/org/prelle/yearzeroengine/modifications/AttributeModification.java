package org.prelle.yearzeroengine.modifications;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.persist.AttributeConverter;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class AttributeModification extends ModificationImpl implements ValueModification<Attribute>, Cloneable {

	@org.prelle.simplepersist.Attribute(required=true)
	@AttribConvert(AttributeConverter.class)
	private Attribute attr;

	@org.prelle.simplepersist.Attribute(name="value",required=true)
	private int value;

    //-----------------------------------------------------------------------
    public AttributeModification() {
     }

    //-----------------------------------------------------------------------
    public AttributeModification(Attribute attr) {
        this.attr = attr;
    }

    //-----------------------------------------------------------------------
    public AttributeModification(Attribute attr, int val) {
        this.attr = attr;
        this.value = val;
    }

    //--------------------------------------------------------------------
    /**
     * @see org.prelle.yearzeroengine.modifications.ModificationImpl#clone()
     */
    public Modification clone() {
    	AttributeModification clone = new AttributeModification(attr, value);
    	clone.expCost = this.expCost;
    	clone.source  = this.source;
    	clone.date    = this.date;
    	return clone;
    }

    //-----------------------------------------------------------------------
    public String toString() {
    	if (attr==null)
    		return "Unknown Attribute";
        return attr.getShortName()+" "+value;
    }

	//-------------------------------------------------------------------
	public Attribute getAttribute() {
		return attr;
	}

    //-----------------------------------------------------------------------
    public void setAttribute(Attribute attr) {
        this.attr = attr;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof AttributeModification) {
            AttributeModification amod = (AttributeModification)o;
            if (amod.getAttribute()!=attr) return false;
            return true;
        } else
            return false;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof AttributeModification) {
            AttributeModification amod = (AttributeModification)o;
            if (amod.getAttribute()!=attr) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof AttributeModification))
            return toString().compareTo(obj.toString());
        AttributeModification other = (AttributeModification)obj;
        if (attr!=other.getAttribute())
            return attr.getName().compareTo(other.getAttribute().getName());
        return 0;
    }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getModifiedItem()
	 */
	@Override
	public Attribute getModifiedItem() {
		return attr;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getValue()
	 */
	@Override
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#setValue(int)
	 */
	@Override
	public void setValue(int value) {
		this.value = value;
	}

}
