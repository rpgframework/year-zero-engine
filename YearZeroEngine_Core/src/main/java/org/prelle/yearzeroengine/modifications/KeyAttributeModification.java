package org.prelle.yearzeroengine.modifications;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.persist.AttributeConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KeyAttributeModification extends ModificationImpl {

	@org.prelle.simplepersist.Attribute(name="ref")
	@AttribConvert(AttributeConverter.class)
	private Attribute attr;
    
    //-----------------------------------------------------------------------
    public KeyAttributeModification() {
     }
    
    //-----------------------------------------------------------------------
    public KeyAttributeModification(Attribute attr) {
        this.attr = attr;
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
    	if (attr==null)
    		return "Unknown Attribute";
        return attr.getShortName();
    }
    //-----------------------------------------------------------------------
    public Attribute getAttribute() {
        return attr;
    }
    
    //-----------------------------------------------------------------------
    public void setAttribute(Attribute attr) {
        this.attr = attr;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof KeyAttributeModification) {
            KeyAttributeModification amod = (KeyAttributeModification)o;
            if (amod.getAttribute()!=attr) return false;
            return true;
        } else
            return false;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof KeyAttributeModification) {
            KeyAttributeModification amod = (KeyAttributeModification)o;
            if (amod.getAttribute()!=attr) return false;
            return true;
        } else
            return false;
    }
    
    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof KeyAttributeModification))
            return toString().compareTo(obj.toString());
        KeyAttributeModification other = (KeyAttributeModification)obj;
        if (attr!=other.getAttribute())
            return attr.getName().compareTo(other.getAttribute().getName());
        return 0;
    }
    
}
