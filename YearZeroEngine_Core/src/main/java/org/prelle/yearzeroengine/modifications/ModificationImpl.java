package org.prelle.yearzeroengine.modifications;

import java.util.Date;
import java.util.UUID;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;


/**
 * @author prelle
 *
 */
public abstract class ModificationImpl implements Modification {
    
	@Attribute(required=false)
	protected Integer expCost;
	@Attribute(required=false)
	protected Date date;
	/**
	 * The object which is responsible for the modification
	 */
	protected transient Object source;
	
	private UUID uuid;
	
    //-----------------------------------------------------------------------
    public ModificationImpl() {
    	uuid = UUID.randomUUID();
    }
	
    //-----------------------------------------------------------------------
    public ModificationImpl(Modification mod) {
    	expCost = mod.getExpCost();
    	date    = mod.getDate();
    	source  = mod.getSource();
//    	uuid    = mod.getUUID();
    }

	//-------------------------------------------------------------------
    public Modification clone() {
    	try {
    		return (Modification) super.clone();
    	} catch ( CloneNotSupportedException e ) {
    		throw new InternalError();
    	}
    }

	//-------------------------------------------------------------------
	/**
	 * @return the expCost
	 */
	public int getExpCost() {
		if (expCost==null)
			return 0;
		return expCost;
	}

	//-------------------------------------------------------------------
	/**
	 * @param expCost the expCost to set
	 */
	public void setExpCost(int expCost) {
		if (expCost==0)
			this.expCost = null;
		else
		this.expCost = expCost;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return date;
	}

	//-------------------------------------------------------------------
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the source
	 */
	public Object getSource() {
		return source;
	}

	//--------------------------------------------------------------------
	/**
	 * @param source the source to set
	 */
	public void setSource(Object source) {
		this.source = source;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.modification.Modification#getUUID()
//	 */
//	@Override
	public UUID getUUID() {
		return uuid;
	}
    
}// Modification

