package org.prelle.yearzeroengine.modifications;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.yearzeroengine.ItemTemplate;
import org.prelle.yearzeroengine.persist.ItemConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ItemModification extends ModificationImpl {

	@Attribute(name="ref")
	@AttribConvert(ItemConverter.class)
	private ItemTemplate item;
	@Attribute(name="i18nkey")
	private String i18nKey;
	@Attribute
	private String name;
	/**
	 * e.g. "WEIGHT=TINY,TIER=ADVANCED"
	 */
	@Attribute
	private String options;

    //-----------------------------------------------------------------------
    public ItemModification() {
     }

    //-----------------------------------------------------------------------
    public ItemModification(ItemTemplate value) {
    	this.item = value;
    }

    //-----------------------------------------------------------------------
    public String toString() {
    	if (item==null)
    		return "Unknown item";
        return item.getName();
    }
    //-----------------------------------------------------------------------
    public ItemTemplate getItem() {
        return item;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof ItemModification) {
            ItemModification amod = (ItemModification)o;
            if (amod.getItem()!=item) return false;
            return (amod.getI18nKey()==i18nKey);
        } else
            return false;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof ItemModification) {
            ItemModification amod = (ItemModification)o;
            if (amod.getItem()!=item) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof ItemModification))
            return toString().compareTo(obj.toString());
        ItemModification other = (ItemModification)obj;
        if (item!=other.getItem())
            return item.getName().compareTo(other.getItem().getName());
        return 0;
    }

	//--------------------------------------------------------------------
	/**
	 * @return the i18nKey
	 */
	public String getI18nKey() {
		return i18nKey;
	}

	//--------------------------------------------------------------------
	/**
	 * @param i18nKey the i18nKey to set
	 */
	public void setI18nKey(String i18nKey) {
		this.i18nKey = i18nKey;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//--------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the options
	 */
	public String getOptions() {
		return options;
	}

	//--------------------------------------------------------------------
	/**
	 * @param options the options to set
	 */
	public void setOptions(String options) {
		this.options = options;
	}

}
