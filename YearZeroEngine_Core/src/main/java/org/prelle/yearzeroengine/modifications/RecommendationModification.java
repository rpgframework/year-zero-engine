/**
 * 
 */
package org.prelle.yearzeroengine.modifications;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class RecommendationModification extends ModificationImpl {
	
	@Attribute
	private String type;
	@Attribute
	private String ref;

	//-------------------------------------------------------------------
	/**
	 */
	public RecommendationModification() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @return Type of reference 
	 */
	public String getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the ref
	 */
	public String getReferencedId() {
		return ref;
	}

}
