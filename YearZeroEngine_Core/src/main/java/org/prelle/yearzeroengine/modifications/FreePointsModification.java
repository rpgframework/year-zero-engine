/**
 *
 */
package org.prelle.yearzeroengine.modifications;

import java.util.Date;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class FreePointsModification implements Modification {

	public enum Type {
		ATTRIBUTES,
		SKILLS,
		TALENTS,
		ENCUMBRANCE,
	}

	@Attribute
	private Type type;
	@Attribute
	private int count;

	//-------------------------------------------------------------------
	public FreePointsModification() {
	}

	//-------------------------------------------------------------------
	public FreePointsModification(Type type, int count) {
		this.type = type;
		this.count= count;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return type+"="+count;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date arg0) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int arg0) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object arg0) {
		// TODO Auto-generated method stub

	}

	public Type getType() {
		return type;
	}

	public int getCount() {
		return count;
	}

}
