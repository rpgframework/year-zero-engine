package org.prelle.yearzeroengine.modifications;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.yearzeroengine.Skill;
import org.prelle.yearzeroengine.persist.SkillConverter;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class SkillModification extends ModificationImpl implements ValueModification<Skill> {

	@org.prelle.simplepersist.Attribute(name="ref")
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	@org.prelle.simplepersist.Attribute(name="val")
	private int value;
   
    //-----------------------------------------------------------------------
    public SkillModification() {
    }
    
    //-----------------------------------------------------------------------
    public SkillModification(Skill skill, int val) {
        this.skill = skill;
        this.value = val;
    }
    
    //-----------------------------------------------------------------------
    public SkillModification(Skill skill, int val, int exp) {
        this.skill = skill;
        this.value = val;
        super.expCost = exp;
    }
    
    //-----------------------------------------------------------------------
    public SkillModification(SkillModification toClone) {
    	super(toClone);
        this.skill = toClone.getSkill();
        this.value = toClone.getValue();
    }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		return new SkillModification(this);
	}
  
    //-----------------------------------------------------------------------
    public String toString() {
    	if (skill==null)
    		return "Unknown skill";
        return skill.getName();
    }
    
    //-----------------------------------------------------------------------
    public Skill getSkill() {
        return skill;
    }
    
    //-----------------------------------------------------------------------
    public boolean equals(Object o) {
        if (o instanceof SkillModification) {
            SkillModification amod = (SkillModification)o;
            if (amod.getSkill()!=skill) return false;
            return amod.getValue()==value;
        } else
            return false;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof SkillModification) {
            SkillModification amod = (SkillModification)o;
            if (amod.getSkill()!=skill) return false;
            return true;
        } else
            return false;
    }
    
    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof SkillModification))
            return toString().compareTo(obj.toString());
        SkillModification other = (SkillModification)obj;
        if (skill!=other.getSkill())
            return skill.getName().compareTo(other.getSkill().getName());
        return 0;
    }

	@Override
	public Skill getModifiedItem() {
		return skill;
	}

	@Override
	public int getValue() {
		return value;
	}

	@Override
	public void setValue(int value) {
		this.value = value;
	}
    
}
