package org.prelle.yearzeroengine.modifications;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.persist.TalentConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class TalentModification extends ModificationImpl {

	@org.prelle.simplepersist.Attribute(name="ref")
	@AttribConvert(TalentConverter.class)
	private Talent talent;
	@org.prelle.simplepersist.Attribute(name="type")
	private Talent.Type type;
//	@org.prelle.simplepersist.Attribute(name="val")
//	private int value;
   
    //-----------------------------------------------------------------------
    public TalentModification() {
     }
    
    //-----------------------------------------------------------------------
    public TalentModification(Talent talent) {
        this.talent = talent;
//        this.value = val;
    }
    
    //-----------------------------------------------------------------------
    public TalentModification(Talent talent, int exp) {
        this.talent = talent;
        this.expCost = exp;
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
    	if (talent==null)
    		return "Unknown talent";
        return talent.getName();
    }
    
    //-----------------------------------------------------------------------
    public Talent getTalent() {
        return talent;
    }
    
    //-------------------------------------------------------------------
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof TalentModification) {
            TalentModification amod = (TalentModification)o;
            if (amod.getTalent()!=talent) return false;
            return true;
        } else
            return false;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof TalentModification) {
            TalentModification amod = (TalentModification)o;
            if (amod.getTalent()!=talent) return false;
            return true;
        } else
            return false;
    }
    
    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof TalentModification))
            return toString().compareTo(obj.toString());
        TalentModification other = (TalentModification)obj;
        if (talent!=other.getTalent())
            return talent.getName().compareTo(other.getTalent().getName());
        return 0;
    }

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Talent.Type getType() {
		return type;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.modification.ValueModification#getModifiedItem()
//	 */
//	@Override
//	public Talent getModifiedItem() {
//		return talent;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.modification.ValueModification#getValue()
//	 */
//	@Override
//	public int getValue() {
//		return value;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.modification.ValueModification#setValue(int)
//	 */
//	@Override
//	public void setValue(int value) {
//		this.value = value;
//	}
    
}
