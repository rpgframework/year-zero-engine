/**
 *
 */
package org.prelle.yearzeroengine.modifications;

import java.util.Date;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CurrencyModification extends ModificationImpl {

	@Attribute
	private int value;

	//-------------------------------------------------------------------
	public CurrencyModification() {
	}

	//-------------------------------------------------------------------
	public CurrencyModification(int count) {
		this.value= count;
	}

	//-------------------------------------------------------------------
	public CurrencyModification(CurrencyModification toClone) {
		super(toClone);
		this.value= toClone.getValue();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Currency +"+value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		// TODO Auto-generated method stub
		return 0;
	}
	   
    //-----------------------------------------------------------------------
    public boolean equals(Object o) {
        if (o instanceof CurrencyModification) {
        	CurrencyModification amod = (CurrencyModification)o;
        	// TODO: compare date
            return amod.getValue()==value;
        } else
            return false;
    }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		return new CurrencyModification(this);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	//--------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

}
