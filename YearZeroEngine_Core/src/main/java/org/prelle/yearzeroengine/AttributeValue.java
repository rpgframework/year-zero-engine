/**
 *
 */
package org.prelle.yearzeroengine;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.modifications.AttributeModification;
import org.prelle.yearzeroengine.persist.AttributeConverter;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name="attr")
public class AttributeValue extends ModifyableImpl implements NumericalValue<Attribute>, Comparable<AttributeValue>, ModifyableValue<Attribute> {

	@org.prelle.simplepersist.Attribute(name="id",required=true)
	@AttribConvert(AttributeConverter.class)
	private Attribute id;

	/**
	 * The recent value of the attribute, including modifications by
	 * race on generation, the distributed points on generation
	 * and the points bought.
	 * Bought = unmodifiedValue - start;
	 */
	@org.prelle.simplepersist.Attribute(name="value",required=true)
	private int distributed;

	/**
	 * The final value of the attribute after generation, before
	 * exp have been spent.
	 * During priority generation this contains the value until
	 * which the attribute is paid by attribute points
	 */
	@org.prelle.simplepersist.Attribute(name="start",required=false)
	private int start;


	//-------------------------------------------------------------------
	public AttributeValue() {
	}

	//-------------------------------------------------------------------
	public AttributeValue(Attribute attr, int val) {
		this.id = attr;
		this.distributed = val;
		this.start = val;
	}

	//-------------------------------------------------------------------
	public AttributeValue(AttributeValue copy) {
		this.id = copy.getModifyable();
		this.start = copy.getStart();
		this.distributed = copy.getPoints();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%s  \tSTART=%d  NORM=%d MOD=%d  VAL=%d mods=%s", id, start, distributed, getModifier(), getModifiedValue(), ""+getModifications());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public Attribute getAttribute() {
		return id;
	}

	//-------------------------------------------------------------------
	public int getModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()==id)
					count += aMod.getValue();
			}
		}
		return count;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	@Override
	public int getModifiedValue() {
		return distributed + getModifier();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public Attribute getModifyable() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AttributeValue other) {
		return id.getName().compareTo(other.getModifyable().getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int distributed) {
		this.distributed = distributed;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the start
	 */
	public int getStart() {
		return start;
	}

	//-------------------------------------------------------------------
	/**
	 * @param start the start to set
	 */
	public void setStart(int start) {
		this.start = start;
	}

	//-------------------------------------------------------------------
	/**
	 * Points gained by investing EXP
	 * @return the v1Value
	 */
	public int getBought() {
		return distributed - start;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return distributed;
	}

}
