/**
 * 
 */
package org.prelle.yearzeroengine;

/**
 * @author prelle
 *
 */
public interface Attribute {

    //-------------------------------------------------------------------
    public String name();
	
	//-------------------------------------------------------------------
	public String getShortName();

    //-------------------------------------------------------------------
    public String getName();

    //-------------------------------------------------------------------
	public boolean isPrimary();

}
