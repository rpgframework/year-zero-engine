/**
 *
 */
package org.prelle.yearzeroengine;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="relation")
public class CharacterRelation {

	@Attribute
	private String to;
	@Attribute
	private String kind;
	@Attribute
	private boolean buddy;

	//-------------------------------------------------------------------
	public CharacterRelation() {
	}

	//-------------------------------------------------------------------
	public CharacterRelation(String to, String kind) {
		this.to = to;
		this.kind = kind;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	//-------------------------------------------------------------------
	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the kind
	 */
	public String getKind() {
		return kind;
	}

	//-------------------------------------------------------------------
	/**
	 * @param kind the kind to set
	 */
	public void setKind(String kind) {
		this.kind = kind;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the buddy
	 */
	public boolean isBuddy() {
		return buddy;
	}

	//--------------------------------------------------------------------
	/**
	 * @param buddy the buddy to set
	 */
	public void setBuddy(boolean buddy) {
		this.buddy = buddy;
	}

}
