/**
 *
 */
package org.prelle.yearzeroengine;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.persist.TalentConverter;

import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name="talentval")
public class TalentValue extends ModifyableImpl implements Comparable<TalentValue> {

	@org.prelle.simplepersist.Attribute(name="id",required=true)
	@AttribConvert(TalentConverter.class)
	private Talent id;
	@org.prelle.simplepersist.Attribute(name="gen")
	private boolean generated;

	//-------------------------------------------------------------------
	public TalentValue() {
	}

	//-------------------------------------------------------------------
	public TalentValue(Talent attr, int val) {
		this.id = attr;
	}

	//-------------------------------------------------------------------
	public TalentValue(TalentValue copy) {
		this.id = copy.getTalent();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%s", id);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public Talent getTalent() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(TalentValue other) {
		return id.getName().compareTo(other.getTalent().getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @return the generated
	 */
	public boolean isGenerated() {
		return generated;
	}

	//--------------------------------------------------------------------
	/**
	 * @param generated the generated to set
	 */
	public void setGenerated(boolean generated) {
		this.generated = generated;
	}

}
