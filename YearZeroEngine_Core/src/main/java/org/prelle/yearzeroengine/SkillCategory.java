/**
 * 
 */
package org.prelle.yearzeroengine;

/**
 * @author prelle
 *
 */
public interface SkillCategory  {

    //-------------------------------------------------------------------
    public String name();
	
	//-------------------------------------------------------------------
	public String getShortName();

    //-------------------------------------------------------------------
    public String getName();

    //-------------------------------------------------------------------
	public int ordinal();

}
