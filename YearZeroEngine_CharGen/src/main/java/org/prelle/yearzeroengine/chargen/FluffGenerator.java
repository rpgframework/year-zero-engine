/**
 *
 */
package org.prelle.yearzeroengine.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.CharacterRelation;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.FluffController;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class FluffGenerator<T extends YearZeroEngineCharacter> implements
		FluffController, CharacterProcessor {

	private final static Logger logger = YZECharGenConstants.LOGGER;
	private final static PropertyResourceBundle RES = YZECharGenConstants.RES;

	protected CommonCharacterGenerator<T> parent;
	protected List<ToDoElement> toDos;

	//--------------------------------------------------------------------
	public FluffGenerator(CommonCharacterGenerator<T> parent) {
		this.parent = parent;
		toDos = new ArrayList<ToDoElement>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model,
			List<Modification> unprocessed) {
		toDos.clear();

		if (parent.getCharacter().getProblem()==null) {
			// Personal problem not defined
			toDos.add(new ToDoElement(Severity.WARNING, RES.getString("todo.fluff.no_personal_problem")));
		}

		if (parent.getCharacter().getRelations().isEmpty()) {
			// Relationships to PCs not definied
			toDos.add(new ToDoElement(Severity.WARNING, RES.getString("todo.fluff.no_relationships")));
		}

		if (parent.getCharacter().getName()==null || parent.getCharacter().getName().isEmpty()) {
			// No character name given yet
			toDos.add(new ToDoElement(Severity.STOPPER, RES.getString("todo.fluff.no_name")));
		}
		return unprocessed;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.FluffController#setName(java.lang.String)
	 */
	@Override
	public void setName(String value) {
		logger.info("Set name: "+value);
		parent.getCharacter().setName(value);
		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.FluffController#setPersonalProblem(java.lang.String)
	 */
	@Override
	public void setPersonalProblem(String value) {
		logger.info("Set personal problem: "+value);
		parent.getCharacter().setProblem(value);
		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.FluffController#addRelation(org.prelle.yearzeroengine.CharacterRelation)
	 */
	@Override
	public void addRelation(CharacterRelation value) {
		logger.info("Add relation: "+value.getTo()+": "+value.getKind());
		parent.getCharacter().getRelations().add(value);
		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.FluffController#removeRelation(org.prelle.yearzeroengine.CharacterRelation)
	 */
	@Override
	public void removeRelation(CharacterRelation value) {
		logger.info("remove relation: "+value.getTo()+": "+value.getKind());
		if (parent.getCharacter().getRelations().remove(value))
			parent.runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.FluffController#getRelations()
	 */
	@Override
	public List<CharacterRelation> getRelations() {
		return parent.getCharacter().getRelations();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.FluffController#setFace(java.lang.String)
	 */
	@Override
	public void setFace(String value) {
		logger.info("Set face: "+value);
		parent.getCharacter().setFace(value);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.FluffController#setClothing(java.lang.String)
	 */
	@Override
	public void setClothing(String value) {
		logger.info("Set clothing: "+value);
		parent.getCharacter().setClothing(value);
		parent.runProcessors();
	}

}
