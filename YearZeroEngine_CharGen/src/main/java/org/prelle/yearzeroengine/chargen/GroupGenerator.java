/**
 * 
 */
package org.prelle.yearzeroengine.chargen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.GroupController;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GroupGenerator<T extends YearZeroEngineCharacter> implements GroupController, CharacterProcessor {

	private final static Logger logger = YZECharGenConstants.LOGGER;
	private final static PropertyResourceBundle RES = YZECharGenConstants.RES;
	
	protected CommonCharacterGenerator<T> parent;
	protected List<ToDoElement> toDos;
	
	protected List<String> pcNames;

	//-------------------------------------------------------------------
	/**
	 */
	public GroupGenerator(CommonCharacterGenerator<T> parent) {
		this.parent = parent;
		toDos = new ArrayList<>();
		pcNames = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.GroupController#setPCNames(java.util.Collection)
	 */
	@Override
	public void setPCNames(Collection<String> names) {
		logger.info("Set PC names to "+names);
		pcNames.clear();
		pcNames.addAll(names);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.GroupController#getPCNames()
	 */
	@Override
	public List<String> getPCNames() {
		return new ArrayList<>(pcNames);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		try {
			toDos.clear();
			
			if (pcNames.isEmpty()) {
				toDos.add(new ToDoElement(Severity.STOPPER, RES.getString("todo.group.names_not_set") ));
			}
		} finally {
			logger.trace("STOP : process");
			
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

}
