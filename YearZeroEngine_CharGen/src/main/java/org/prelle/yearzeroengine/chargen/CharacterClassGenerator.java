/**
 *
 */
package org.prelle.yearzeroengine.chargen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PropertyResourceBundle;
import java.util.Random;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.CharacterClass;
import org.prelle.yearzeroengine.CharacterRelation;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;
import org.prelle.yearzeroengine.charctrl.CharacterClassController;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventType;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;
import org.prelle.yearzeroengine.modifications.ModificationChoice;
import org.prelle.yearzeroengine.modifications.TalentModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.DecisionToMake;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CharacterClassGenerator<T extends YearZeroEngineCharacter> implements CharacterClassController, CharacterProcessor {

	private final static Logger logger = YZECharGenConstants.LOGGER;

	protected CommonCharacterGenerator<T> parent;
	protected YearZeroEngineRuleset ruleset;
	protected PropertyResourceBundle i18n;
	protected List<ToDoElement> toDos;
	protected Map<Modification, DecisionToMake> decisions;
	protected List<Modification> decisionKeyList;

	protected Attribute keyAttr;

	//-------------------------------------------------------------------
	public CharacterClassGenerator(CommonCharacterGenerator<T> parent, YearZeroEngineRuleset ruleset, PropertyResourceBundle i18n) {
		this.parent = parent;
		this.i18n = i18n;
		this.ruleset = ruleset;
		decisions    = new HashMap<>();
		decisionKeyList = new ArrayList<>();
		toDos = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterClassController#getAvailableClasses()
	 */
	@Override
	public List<CharacterClass> getAvailableClasses() {
		return ruleset.getCharacterClasses();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterClassController#getSelected()
	 */
	@Override
	public CharacterClass getSelected() {
		return parent.getCharacter().getCharacterClass();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterClassController#setSelected(org.prelle.yearzeroengine.CharacterClass)
	 */
	@Override
	public void setSelected(CharacterClass value) {
		if (!getAvailableClasses().contains(value))
			throw new IllegalArgumentException("Not available: "+value+"  avail="+getAvailableClasses());
		logger.info("Select character class "+value);
		parent.getCharacter().setCharacterClass(value);
		decisions.clear();
		decisionKeyList.clear();

		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterClassController#rollRandomRelations(org.prelle.yearzeroengine.YearZeroEngineCharacter)
	 */
	@Override
	public void rollRandomRelations(YearZeroEngineCharacter model) {
		if (model.getCharacterClass()==null) {
			logger.warn("Cannot randomize relations - character class not selected yet");
			return;
		}

		List<CharacterRelation> toSet = new ArrayList<>();
		Random random = new Random();
		List<String> options = new ArrayList<>();
		if (options.isEmpty()) {
			options = model.getCharacterClass().getFluffOptions("relatePC");
		}
		if (options.isEmpty()) {
			logger.error("No relation options found for "+model.getCharacterClass().getId());
			return;
		}
		for (String name : parent.getGroupController().getPCNames()) {
			// Roll from remaining options
			int roll = random.nextInt(options.size());
			String rolled = options.get(roll);
			options.remove(rolled);
			toSet.add( new CharacterRelation(name, rolled) );
		}
		model.setRelations(toSet);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterClassController#rollRandomFace(org.prelle.yearzeroengine.YearZeroEngineCharacter)
	 */
	@Override
	public void rollRandomFace(YearZeroEngineCharacter model) {
		if (model.getCharacterClass()==null) {
			logger.warn("Cannot randomize face - character class not selected yet");
			return;
		}

		List<String> options = model.getCharacterClass().getFluffOptions("appearance.face");
		if (options==null) {
			logger.warn("No face options found for character class "+model.getCharacterClass().getId());
			return;
		}

		if (options.isEmpty()) {
			logger.error("No face options found for "+model.getCharacterClass().getId());
			return;
		}
		model.setFace( options.get((new Random()).nextInt(options.size())));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, model));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterClassController#rollRandomClothing(org.prelle.yearzeroengine.YearZeroEngineCharacter)
	 */
	@Override
	public void rollRandomClothing(YearZeroEngineCharacter model) {
		if (model.getCharacterClass()==null) {
			logger.warn("Cannot randomize clothing - character class not selected yet");
			return;
		}

		List<String> options = model.getCharacterClass().getFluffOptions("appearance.clothing");
		if (options==null) {
			logger.warn("No clothing options found for character class "+model.getCharacterClass().getId());
			return;
		}

		if (options.isEmpty()) {
			logger.error("No clothing options found for "+model.getCharacterClass().getId());
			return;
		}
		model.setClothing( options.get((new Random()).nextInt(options.size())));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, model));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>(previous);
		toDos.clear();
		try {
			if (getSelected()==null) {
				toDos.add(new ToDoElement(Severity.STOPPER, i18n.getString("todo.characterclass_not_selected")));
				decisions.clear();
			} else {
				logger.info("Select character class: "+getSelected().getId());
//				// Remove those DecisionToMake not related to collection
//				for (Modification mod : new ArrayList<>(decisions.keySet())) {
//					if (!getSelected().getModifications().contains(mod)) {
//						decisions.remove(mod);
//						decisionKeyList.remove(mod);
//					}
//				}
				/*
				 * Check if any ModificationChoice has been added
				 */
				for (Modification mod : getSelected().getModifications()) {
					if (mod instanceof ModificationChoice) {
						DecisionToMake decision = decisions.get(mod);
						if (decision==null) {
							// Yet unknown choice
							logger.debug(" new decision to make: "+mod);
							decision = new DecisionToMake(mod, getSelected().getName());
							decisions.put(mod, decision);
							decisionKeyList.add(mod);
						}
					} else
						unprocessed.add(mod);
				}

				// Handle decisions
				for (Entry<Modification, DecisionToMake> entry : decisions.entrySet()) {
					List<Modification> decMods = entry.getValue().getDecision();
					if (decMods==null) {
						logger.debug(" Yet to decide: "+entry.getValue().getChoice());
						toDos.add(new ToDoElement(Severity.STOPPER, String.format(i18n.getString("todo.decisiontomake"), entry.getValue().getChoice(), entry.getKey().getSource())));
					} else {
						logger.debug(" Decided: "+entry.getValue().getChoice()+" is "+entry.getValue().getDecision());
						for (Modification tmp : decMods) {
						boolean skipIt = (tmp instanceof TalentModification) && (((TalentModification)tmp).getTalent()==null);
						if (skipIt)
							logger.debug("Don't send down the chain: "+tmp);
						else
							unprocessed.add(tmp);
						}
					}
				}

				// Define relations to other player characters
				if (model.getRelations().isEmpty()) {
					rollRandomRelations(model);
				}
				// Define face
				if (model.getFace()==null) {
					rollRandomFace(model);
				}
				// Define clothing
				if (model.getClothing()==null) {
					rollRandomClothing(model);
				}
			}
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterClassController#getDecisions()
	 */
	@Override
	public List<DecisionToMake> getDecisions() {
		List<DecisionToMake> ret = new ArrayList<>();
		for (Modification mod : decisionKeyList) {
			if (decisions.get(mod)==null) {
				logger.error("No entry in decisions for "+mod);

			}
			ret.add(decisions.get(mod));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterClassController#decide(org.prelle.yearzeroengine.chargen.DecisionToMake, de.rpgframework.genericrpg.modification.Modification)
	 */
	@Override
	public void decide(DecisionToMake choice, Modification mod) {
		logger.info("For choice "+choice.getChoice()+" decide for "+mod);
		// Clear all decisions that have been made because of a previous choice
		if (choice.getDecision()!=null) {
			Modification old = choice.getDecision().get(0);
			decisionKeyList.remove(old);
			DecisionToMake oldDec = decisions.remove(old);
			if (oldDec!=null) {
				logger.debug("  remove previously newly created decision "+oldDec);
			}
		}
		choice.setDecision(Arrays.asList(mod));

		if (mod instanceof TalentModification && ((TalentModification)mod).getType()!=null) {
			logger.info("Decided for a talent that requires another choice");
			ModificationChoice options = new ModificationChoice();
			for (Talent tal : ruleset.getListByType(Talent.class)) {
				if (tal.getType()==((TalentModification)mod).getType())
					options.add(new TalentModification(tal));
			}
			DecisionToMake newDec = new DecisionToMake(options, "");
			logger.debug(" new decision to make: "+newDec);
			decisions.put(mod, newDec);
			decisionKeyList.add(mod);
		}
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the toDos
	 */
	public List<ToDoElement> getToDos() {
		return toDos;
	}

}
