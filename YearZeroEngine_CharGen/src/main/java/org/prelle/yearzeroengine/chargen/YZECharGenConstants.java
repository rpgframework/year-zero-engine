/**
 * 
 */
package org.prelle.yearzeroengine.chargen;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Stefan Prelle
 *
 */
public interface YZECharGenConstants {

	public final static Logger LOGGER = LogManager.getLogger("yearzero.chargen");
	public final static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/yearzeroengine/chargen/i18n/chargen");

}
