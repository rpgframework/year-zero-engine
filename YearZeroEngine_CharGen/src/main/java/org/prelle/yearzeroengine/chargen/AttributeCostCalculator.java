/**
 *
 */
package org.prelle.yearzeroengine.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class AttributeCostCalculator implements CharacterProcessor {

	private final static Logger logger = YZECharGenConstants.LOGGER;
	private final static PropertyResourceBundle RES = YZECharGenConstants.RES;

	private int toSpend;
	private int left;
	private List<ToDoElement> toDos;

	//-------------------------------------------------------------------
	public AttributeCostCalculator() {
		toDos = new ArrayList<ToDoElement>();
	}

	//-------------------------------------------------------------------
	public void setPointsToSpend(int toSpend) {
		this.toSpend = toSpend;
	}

	//-------------------------------------------------------------------
	public int getPointsLeft() {
		return left;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		toDos.clear();
		try {
			left = toSpend;
			for (Attribute key : model.getRuleset().getAttributes()) {
				if (key.isPrimary()) {
					AttributeValue val = model.getAttribute(key);
					left -= val.getBought();
				}
			}
			logger.debug("Spent "+(toSpend-left)+" points on attributes");
		} finally {
			logger.trace("STOP : process");
		}

		if (left>0)
			toDos.add(new ToDoElement(Severity.STOPPER, String.format(RES.getString("todo.attribute.spendmore"), left)));
		else if (left<0)
			toDos.add(new ToDoElement(Severity.STOPPER, String.format(RES.getString("todo.attribute.spendless"), left)));

		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

}
