/**
 *
 */
package org.prelle.yearzeroengine.chargen.event;

/**
 * @author prelle
 *
 */
public enum GenerationEventType {

	/** Key is the archetype */
	ARCHETYPE_CHANGED,

	/** Key is Attribute, Value is AttributeValue */
	ATTRIBUTE_CHANGED,
	/** Key is Skill, Value is distributed points */
	SKILL_CHANGED,

	/** Key is the archetype */
	ICON_CHANGED,

	/** Key is the archetype */
	CHARACTER_CHANGED,
	
	/** Experience */
	EXPERIENCE_CHANGED,

}
