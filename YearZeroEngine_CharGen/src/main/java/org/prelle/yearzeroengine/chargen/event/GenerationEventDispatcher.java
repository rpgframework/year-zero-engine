/**
 *
 */
package org.prelle.yearzeroengine.chargen.event;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.chargen.YZECharGenConstants;

/**
 * @author prelle
 *
 */
public class GenerationEventDispatcher {

	private final static Logger logger = YZECharGenConstants.LOGGER;

	private static Collection<GenerationEventListener> listener;

	private static boolean blockCharacterChanged;

	//--------------------------------------------------------------------
	static {
		listener = new ArrayList<GenerationEventListener>();
	}

	//--------------------------------------------------------------------
	public static void addListener(GenerationEventListener callback) {
		if (!listener.contains(callback))
			listener.add(callback);
	}

	//--------------------------------------------------------------------
	public static void removeListener(GenerationEventListener callback) {
		listener.remove(callback);
	}

	//--------------------------------------------------------------------
	public static void clear() {
		listener.clear();
	}

	//--------------------------------------------------------------------
	public static void fireEvent(GenerationEvent event) {
		logger.debug("fire "+event.getType());
		if (event.getType()==GenerationEventType.CHARACTER_CHANGED && blockCharacterChanged)
			return;
//		try {
//			throw new RuntimeException("Trace");
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		for (GenerationEventListener callback : listener) {
			try {
				callback.handleGenerationEvent(event);
			} catch (Exception e) {
				logger.error("Error delivering generation event",e);
			}
		}
	}

	//--------------------------------------------------------------------
	public static void fireEvent(GenerationEventType type, Object key) {
		fireEvent(new GenerationEvent(type, key));
	}

	//--------------------------------------------------------------------
	public static void fireCharacterChange(YearZeroEngineCharacter model) {
		fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, model));
	}

	//--------------------------------------------------------------------
	public static void setBlockCharacterChanged(boolean value) {
		blockCharacterChanged = value;
	}

}
