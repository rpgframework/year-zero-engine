/**
 *
 */
package org.prelle.yearzeroengine.chargen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.AttributeController;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;
import org.prelle.yearzeroengine.modifications.AttributeModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification.Type;
import org.prelle.yearzeroengine.modifications.KeyAttributeModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class AttributeGenerator<T extends YearZeroEngineCharacter> implements AttributeController, CharacterProcessor {

	private final static Logger logger = YZECharGenConstants.LOGGER;

	protected CommonCharacterGenerator<T> parent;
	protected AttributeCostCalculator cost;
	protected List<ToDoElement> toDos;

	protected Attribute keyAttr;

	//-------------------------------------------------------------------
	public AttributeGenerator(CommonCharacterGenerator<T> parent) {
		this.parent = parent;
		cost = new AttributeCostCalculator();
		toDos = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public AttributeCostCalculator getCostCalculator() {
		return cost;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.AttributeController#getMaximum(org.prelle.yearzeroengine.Attribute)
	 */
	@Override
	public int getMaximum(Attribute key) {
		if (key==keyAttr)
			return 5;
		return 4;
	}


	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.AttributeController#getPointsToSpend()
	 */
	@Override
	public int getPointsToSpend() {
		return cost.getPointsLeft();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.AttributeController#canBeIncreased(org.prelle.yearzeroengine.Attribute)
	 */
	public boolean canBeIncreased(AttributeValue val) {
		return (val.getBought()<getMaximum(val.getModifyable()) && getPointsToSpend()>0);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeDecreased(AttributeValue val) {
		return (val.getBought()>1);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.AttributeController#increase(org.prelle.yearzeroengine.Attribute)
	 */
	@Override
	public boolean increase(AttributeValue val) {
		if (!canBeIncreased(val)) {
			logger.warn("Trying to increase attribute "+val.getModifyable()+" which cannot be increased");
			return false;
		}

		val.setPoints(val.getPoints()+1);
		logger.info("Increase "+val.getModifyable()+" to "+val.getPoints());

		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.AttributeController#decrease(org.prelle.yearzeroengine.Attribute)
	 */
	@Override
	public boolean decrease(AttributeValue val) {
		if (!canBeDecreased(val)) {
			logger.warn("Trying to decrease attribute "+val.getModifyable()+" which cannot be increased");
			return false;
		}

		val.setPoints(val.getPoints()-1);
		logger.info("Decrease "+val.getModifyable()+" to "+val.getPoints());

		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();
		toDos.clear();
		try {
			for (Modification tmp : previous) {
				if (tmp instanceof FreePointsModification) {
					FreePointsModification mod = (FreePointsModification)tmp;
					if (mod.getType()==Type.ATTRIBUTES) {
						cost.setPointsToSpend(mod.getCount());
						logger.debug(" Assume to have "+cost.getPointsLeft()+" points for attributes");
						continue;
					}
				} else if (tmp instanceof KeyAttributeModification) {
					KeyAttributeModification mod = (KeyAttributeModification)tmp;
					keyAttr = mod.getAttribute();
					logger.debug(" Key attribute is "+keyAttr);
					continue;
				} else if (tmp instanceof AttributeModification) {
					AttributeModification mod = (AttributeModification)tmp;
					logger.debug(" Apply "+mod+" from "+mod.getSource());
					AttributeValue aVal = parent.getCharacter().getAttribute(mod.getAttribute());
					aVal.addModification(mod.clone());
					continue;
				}
				unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

}
