/**
 *
 */
package org.prelle.yearzeroengine.chargen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.Skill;
import org.prelle.yearzeroengine.SkillValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.SkillController;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;
import org.prelle.yearzeroengine.modifications.FreePointsModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification.Type;
import org.prelle.yearzeroengine.modifications.KeySkillModification;
import org.prelle.yearzeroengine.modifications.SkillModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SkillGenerator<T extends YearZeroEngineCharacter> implements SkillController, CharacterProcessor {

	private final static Logger logger = YZECharGenConstants.LOGGER;

	protected CommonCharacterGenerator<T> parent;
	protected SkillCostCalculator cost;
	protected List<ToDoElement> toDos;

	protected List<Skill> conceptSkills;

	//-------------------------------------------------------------------
	public SkillGenerator(CommonCharacterGenerator<T> parent) {
		this.parent = parent;
		cost = new SkillCostCalculator();
		toDos = new ArrayList<>();
		conceptSkills = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public SkillCostCalculator getCostCalculator() {
		return cost;
	}

	//-------------------------------------------------------------------
	protected int getMaximum(SkillValue key) {
		return conceptSkills.contains(key.getSkill())?3:1;
	}


	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.SkillController#getPointsToSpend()
	 */
	@Override
	public int getPointsToSpend() {
		return cost.getPointsLeft();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.SkillController#canBeIncreased(org.prelle.yearzeroengine.Skill)
	 */
	@Override
	public boolean canBeIncreased(SkillValue val) {
		return (((SkillValue)val).getBought()<getMaximum(((SkillValue)val)) && getPointsToSpend()>0);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.SkillController#canBeDecreased(org.prelle.yearzeroengine.Skill)
	 */
	@Override
	public boolean canBeDecreased(SkillValue val) {
		return (((SkillValue)val).getBought()>0);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.SkillController#increase(org.prelle.yearzeroengine.Skill)
	 */
	@Override
	public boolean increase(SkillValue val) {
		if (!canBeIncreased(val)) {
			logger.warn("Trying to increase attribute "+val+" which cannot be increased");
			return false;
		}

		val.setPoints(val.getPoints()+1);
		logger.info("Increase "+val.getModifyable()+" to "+val.getPoints());

		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.SkillController#decrease(org.prelle.yearzeroengine.Skill)
	 */
	@Override
	public boolean decrease(SkillValue val) {
		if (!canBeDecreased(val)) {
			logger.warn("Trying to increase attribute "+val+" which cannot be increased");
			return false;
		}

		val.setPoints(val.getPoints()-1);
		logger.info("Decrease "+val.getModifyable()+" to "+val.getPoints());

		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();
		toDos.clear();
		conceptSkills.clear();
		try {
			for (Modification tmp : previous) {
				if (tmp instanceof FreePointsModification) {
					FreePointsModification mod = (FreePointsModification)tmp;
					if (mod.getType()==Type.SKILLS) {
						cost.setPointsToSpend(mod.getCount());
						logger.debug(" Assume to have "+cost.getPointsLeft()+" points for skills");
						continue;
					}
				} else if (tmp instanceof KeySkillModification) {
					KeySkillModification mod = (KeySkillModification)tmp;
					conceptSkills.add(mod.getSkill());
					logger.debug(" Add "+mod.getSkill()+" as concept skill");
					continue;
				} else if (tmp instanceof SkillModification) {
					SkillModification mod = (SkillModification)tmp;
					logger.debug(" Apply "+mod);
					SkillValue aVal = parent.getCharacter().getSkillValue(mod.getSkill());
					aVal.addModification(mod);
					continue;
				}
				unprocessed.add(tmp);
			}

			/*
			 * Check that skill values are within boundaries
			 */
			for (SkillValue val : parent.getCharacter().getSkills()) {
				if (val.getPoints()>getMaximum(val)) {
					// Don't warn - just reduce
					val.setPoints(getMaximum(val));
//					toDos.add(new ToDoElement(Severity.STOPPER, "Reduce skill "+val.getModifyable()));
				}
			}
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.SkillController#isConceptSkill(org.prelle.yearzeroengine.Skill)
	 */
	@Override
	public boolean isConceptSkill(Skill value) {
		return conceptSkills.contains(value);
	}

}
