/**
 *
 */
package org.prelle.yearzeroengine.chargen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.TalentController;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;
import org.prelle.yearzeroengine.modifications.FreePointsModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification.Type;
import org.prelle.yearzeroengine.modifications.TalentModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.DecisionToMake;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public abstract class TalentGenerator<T extends YearZeroEngineCharacter> implements TalentController, CharacterProcessor {

	private final static Logger logger = YZECharGenConstants.LOGGER;
	private final static PropertyResourceBundle RES = YZECharGenConstants.RES;

	protected CommonCharacterGenerator<T> parent;
	protected TalentCostCalculator cost;
	protected List<ToDoElement> toDos;
	protected List<Talent> available;
	protected Map<Modification, DecisionToMake> decisions;
	protected List<Modification> decisionKeyList;
	protected int free;
	protected List<TalentValue> freelyChoosen;

	//-------------------------------------------------------------------
	public TalentGenerator(CommonCharacterGenerator<T> parent) {
		this.parent = parent;
		cost = new TalentCostCalculator();
		toDos = new ArrayList<>();
		decisions    = new HashMap<>();
		decisionKeyList = new ArrayList<>();
		available = new ArrayList<>();
		freelyChoosen = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public TalentCostCalculator getCostCalculator() {
		return cost;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#getPointsToSpend()
	 */
	@Override
	public int getPointsToSpend() {
		return cost.getPointsLeft();
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.yearzeroengine.charctrl.TalentController#canBeIncreased(org.prelle.yearzeroengine.Talent)
//	 */
//	@Override
//	public boolean canBeSelected(Talent key) {
//		return false;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.yearzeroengine.charctrl.TalentController#canBeDecreased(org.prelle.yearzeroengine.Talent)
//	 */
//	@Override
//	public boolean canBeDeselected(TalentValue val) {
//		return false;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#select(org.prelle.yearzeroengine.Talent)
	 */
	@Override
	public boolean select(Talent key) {
		if (!canBeSelected(key)) {
			logger.warn("Trying to select talent "+key+" which cannot be selected");
			return false;
		}

		TalentValue val = new TalentValue(key, 0);
		val.setGenerated(true);
		freelyChoosen.add(val);
		logger.info("Select "+val.getTalent());

		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#decrease(org.prelle.yearzeroengine.Talent)
	 */
	@Override
	public boolean deselect(TalentValue val) {
		if (!canBeDeselected(val)) {
			logger.warn("Trying to deselect talent "+val+" which cannot be deselected");
			return false;
		}

		parent.getCharacter().removeTalent(val);

		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> previous) {
		logger.trace("START: process "+previous);
		List<Modification> unprocessed = new ArrayList<>();
		toDos.clear();
		free = 0;

		available.clear();
		available.addAll(model.getRuleset().getListByType(Talent.class).stream().filter(t -> canBeSelected(t)).collect(Collectors.toList()));
		try {
			// Clear all previously selected talents
			for (TalentValue tmp : new ArrayList<TalentValue>(parent.getCharacter().getTalents())) {
					parent.getCharacter().removeTalent(tmp);
			}

			for (Modification tmp : previous) {
				if (tmp instanceof TalentModification) {
					TalentModification mod = (TalentModification)tmp;
					logger.debug(" Apply "+mod.getTalent()+" from "+mod.getSource());
					if (mod.getTalent()==null) {
						logger.error("Trying to apply a not yet selected talent of type "+mod.getType());
						continue;
					}
					boolean alreadyExists = parent.getCharacter().getTalentValue(mod.getTalent())!=null;
					if (!alreadyExists) {
						TalentValue toAdd = new TalentValue(mod.getTalent(), 0);
						toAdd.setGenerated(true);
						parent.getCharacter().addTalent(toAdd);
					} else {
						logger.warn("Trying to apply talent that already exists in character");
					}
					continue;
				} else if (tmp instanceof FreePointsModification && ((FreePointsModification)tmp).getType()==Type.TALENTS) {
					free += ((FreePointsModification)tmp).getCount();
					logger.debug("* Grant "+((FreePointsModification)tmp).getCount()+" talents to choose");
				}
				unprocessed.add(tmp);
			}

			// Count and reaply chosen talents
			for (TalentValue val : freelyChoosen) {
				free--;
				parent.getCharacter().addTalent(val);
			}

			if (free<0) {
				toDos.add(new ToDoElement(Severity.STOPPER, String.format(RES.getString("todo.talent.spendless"), -free)));
			} else if (free>0) {
				toDos.add(new ToDoElement(Severity.STOPPER, String.format(RES.getString("todo.talent.spendmore"), free)));
			}
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#getAvailableTalents()
	 */
	@Override
	public List<Talent> getAvailableTalents() {
		return available;
	}

}
