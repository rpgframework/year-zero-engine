/**
 *
 */
package org.prelle.yearzeroengine.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;
import org.prelle.yearzeroengine.charctrl.AttributeController;
import org.prelle.yearzeroengine.charctrl.CharacterClassController;
import org.prelle.yearzeroengine.charctrl.CharacterController;
import org.prelle.yearzeroengine.charctrl.FluffController;
import org.prelle.yearzeroengine.charctrl.GearController;
import org.prelle.yearzeroengine.charctrl.GroupController;
import org.prelle.yearzeroengine.charctrl.SkillController;
import org.prelle.yearzeroengine.charctrl.TalentController;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventType;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public abstract class CommonCharacterGenerator<T extends YearZeroEngineCharacter> implements CharacterController<T> {

	private final static Logger logger = YZECharGenConstants.LOGGER;

	protected T model;
	private boolean alreadyRunning;

	protected GroupController group;
	protected AttributeController attributes;
	protected CharacterClassController charclasses;
	protected SkillController skills;
	protected TalentController talents;
	protected GearController gear;
	protected FluffController fluff;

	protected List<CharacterProcessor> processChain;
//	protected Map<Modification, DecisionToMake> decisions;

	protected List<Modification> startModifications;

	//-------------------------------------------------------------------
	public CommonCharacterGenerator(YearZeroEngineRuleset ruleset, PropertyResourceBundle chargenI18n) {
		attributes   = new AttributeGenerator<T>(this);
		skills       = new SkillGenerator<T>(this);
//		talents      = new TalentGenerator<>(this);
		fluff        = new FluffGenerator<>(this);
		charclasses  = new CharacterClassGenerator<T>(this, ruleset, chargenI18n);
		processChain = new ArrayList<>();
//		decisions    = new HashMap<>();
		startModifications = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getCharacter()
	 */
	@Override
	public T getCharacter() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getRuleset()
	 */
	@Override
	public YearZeroEngineRuleset getRuleset() {
		return model.getRuleset();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getGroupController()
	 */
	@Override
	public GroupController getGroupController() {
		return group;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return attributes;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		return skills;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getTalentController()
	 */
	@Override
	public TalentController getTalentController() {
		return talents;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getGearController()
	 */
	@Override
	public GearController getGearController() {
		return gear;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getFluffController()
	 */
	@Override
	public FluffController getFluffController() {
		return fluff;
	}

	//-------------------------------------------------------------------
	public CharacterClassController getCharacterClassController() {
		return charclasses;
	}

	//-------------------------------------------------------------------
	public void addUnitTestModification(Modification mod) {
		startModifications.add(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	public void removeUnitTestModification(Modification mod) {
		startModifications.remove(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	public void runProcessors() {
		if (alreadyRunning) {
			logger.warn("runProcessors is already running");
//			System.exit(0);
			return;
		}
		GenerationEventDispatcher.setBlockCharacterChanged(true);

		logger.debug("\n\nSTART: runProcessors: "+processChain.size()+"-------------------------------------------------------");
		alreadyRunning = true;
		try {
			List<Modification> unprocessed = new ArrayList<>(startModifications);
			logger.info("Start mods  = "+unprocessed);
			for (CharacterProcessor processor : processChain) {
				unprocessed = processor.process(model, unprocessed);
				logger.debug("------after "+processor.getClass().getSimpleName()+"     "+unprocessed);
//			/*
//			 * Check if any ModificationChoice has been added
//			 */
//			for (Modification mod : unprocessed) {
//				if (mod instanceof ModificationChoice) {
//					DecisionToMake decision = decisions.get(mod);
//					if (decision==null) {
//						// Yet unknown choice
//						logger.debug(" new decision to make: "+mod);
//						decision = new DecisionToMake(mod);
//						decisions.put(mod, decision);
//					}
//					unprocessed.remove(mod);
//				}
//			}

			}
			logger.info("Remaining mods  = "+unprocessed);
//		logger.info("Remaining karma = "+model.getKarmaFree());
		logger.info("ToDos = "+getToDos());
		GenerationEventDispatcher.setBlockCharacterChanged(false);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, model));
		} finally {
			logger.info("STOP : runProcessors: "+processChain.size()+"-------------------------------------------------------");
			alreadyRunning = false;
		}
	}

	//-------------------------------------------------------------------
	public List<ToDoElement> getToDos() {
		List<ToDoElement> ret = new ArrayList<>();
		for (CharacterProcessor proc : processChain) {
			ret.addAll(proc.getToDos());
		}
		return ret;
	}


	//-------------------------------------------------------------------
	/**
	 * Clear character and start creation
	 */
	public abstract void start();

	//-------------------------------------------------------------------
	/**
	 * Stop generation mode and insert levelling mode
	 */
	public abstract void stop();

}
