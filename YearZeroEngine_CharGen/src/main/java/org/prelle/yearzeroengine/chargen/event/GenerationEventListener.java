/**
 * 
 */
package org.prelle.yearzeroengine.chargen.event;

/**
 * @author prelle
 *
 */
public interface GenerationEventListener {

	//-------------------------------------------------------------------
	public void handleGenerationEvent(GenerationEvent event);
	
}
