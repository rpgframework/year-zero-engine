/**
 * 
 */
package org.prelle.yearzeroengine.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class TalentCostCalculator implements CharacterProcessor {

	private final static Logger logger = YZECharGenConstants.LOGGER;
	private final static PropertyResourceBundle RES = YZECharGenConstants.RES;
	
	private int toSpend;
	private int left;
	private List<ToDoElement> toDos;
	
	//-------------------------------------------------------------------
	public TalentCostCalculator() {
		toDos = new ArrayList<ToDoElement>();
	}

	//-------------------------------------------------------------------
	public void setPointsToSpend(int toSpend) {
		this.toSpend = toSpend;
	}

	//-------------------------------------------------------------------
	public int getPointsLeft() {
		return left;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		toDos.clear();
		try {
			left = toSpend;
//			for (TalentValue key : model.getTalents()) {
//				left --;
//			}
			logger.debug("Spent "+(toSpend-left)+" points on skills");
		} finally {
			logger.trace("STOP : process");
		}
		
		if (left>0)
			toDos.add(new ToDoElement(Severity.STOPPER, String.format(RES.getString("todo.talent.spendmore"), left)));
		else if (left<0)
			toDos.add(new ToDoElement(Severity.STOPPER, String.format(RES.getString("todo.talent.spendless"), left)));
		
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

}
