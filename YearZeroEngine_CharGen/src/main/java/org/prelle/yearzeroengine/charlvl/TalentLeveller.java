/**
 *
 */
package org.prelle.yearzeroengine.charlvl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.TalentController;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class TalentLeveller<T extends YearZeroEngineCharacter> implements TalentController, CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("yearzero.charlvl");

	protected CommonCharacterLeveller<T> parent;
//	protected TalentCostCalculator cost;
	protected List<ToDoElement> toDos;
	protected List<Talent> available;

	//--------------------------------------------------------------------
	public TalentLeveller(CommonCharacterLeveller<T> parent) {
		this.parent = parent;
//		cost = new TalentCostCalculator();
		toDos = new ArrayList<>();
		available = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model,
			List<Modification> previous) {
		logger.trace("START: process "+previous);
		List<Modification> unprocessed = new ArrayList<>();
		toDos.clear();
		available.clear();
		try {
			available.addAll(model.getRuleset().getListByType(Talent.class).stream().filter(t -> canBeSelected(t)).collect(Collectors.toList()));
			logger.debug("List of avaiable talents is now "+available.size()+" elements");
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#getAvailableTalents()
	 */
	@Override
	public List<Talent> getAvailableTalents() {
		return available;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#getPointsToSpend()
	 */
	@Override
	public int getPointsToSpend() {
		return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#canBeSelected(org.prelle.yearzeroengine.Talent)
	 */
	@Override
	public boolean canBeSelected(Talent key) {
		if (key==null)
			return false;
		for (TalentValue val : parent.getCharacter().getTalents()) {
			if (val.getTalent()==key)
				return false;
		}
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#canBeDeselected(org.prelle.yearzeroengine.TalentValue)
	 */
	@Override
	public boolean canBeDeselected(TalentValue key) {
		if (key==null)
			return false;
		return parent.getCharacter().getTalents().contains(key);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#select(org.prelle.yearzeroengine.Talent)
	 */
	@Override
	public boolean select(Talent key) {
		if (!canBeSelected(key)) {
			logger.warn("Trying to select talent "+key+" which cannot be selected");
			return false;
		}

		YearZeroEngineCharacter model = parent.getCharacter();

		TalentValue val = new TalentValue(key, 0);
		model.addTalent(val);
		logger.info("Selected talent "+key);

		// Pay exp
		model.setExpFree(model.getExpFree()-5);
		model.setExpInvested(model.getExpInvested()+5);

		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#deselect(org.prelle.yearzeroengine.TalentValue)
	 */
	@Override
	public boolean deselect(TalentValue key) {
		if (!canBeDeselected(key)) {
			logger.warn("Trying to deselect talent "+key+" which cannot be deselected");
			return false;
		}

		YearZeroEngineCharacter model = parent.getCharacter();

		model.removeTalent(key);
		logger.info("Deselected talent "+key);

		// Pay exp
		model.setExpFree(model.getExpFree()+5);
		model.setExpInvested(model.getExpInvested()-5);

		parent.runProcessors();
		return true;
	}

}
