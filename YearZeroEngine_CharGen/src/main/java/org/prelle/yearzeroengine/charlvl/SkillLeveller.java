/**
 *
 */
package org.prelle.yearzeroengine.charlvl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.Skill;
import org.prelle.yearzeroengine.SkillValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.SkillController;
import org.prelle.yearzeroengine.chargen.SkillCostCalculator;
import org.prelle.yearzeroengine.chargen.YZECharGenConstants;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;
import org.prelle.yearzeroengine.modifications.KeySkillModification;
import org.prelle.yearzeroengine.modifications.SkillModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class SkillLeveller<T extends YearZeroEngineCharacter> implements SkillController, CharacterProcessor {

	private final static Logger logger = YZECharGenConstants.LOGGER;

	protected CommonCharacterLeveller<T> parent;
	protected SkillCostCalculator cost;
	protected List<ToDoElement> toDos;

	protected List<Skill> conceptSkills;

	//--------------------------------------------------------------------
	/**
	 */
	public SkillLeveller(CommonCharacterLeveller<T> parent) {
		this.parent = parent;
		toDos = new ArrayList<>();
		conceptSkills = new ArrayList<>();
		
		logger.warn("mods = "+parent.getCharacter().getCharacterClass().getModifications());
		for (Modification tmp : parent.getCharacter().getCharacterClass().getModifications()) {
			if (tmp instanceof KeySkillModification) {
				KeySkillModification mod = (KeySkillModification)tmp;
				conceptSkills.add(mod.getSkill());
				System.err.println("<init> Add "+mod.getSkill()+" as concept skill");
			}
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeIncreased(SkillValue value) {
		return value.getPoints()<5 && parent.getCharacter().getExpFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeDecreased(SkillValue value) {
		return value.getPoints()>0 && value.getPoints()>value.getStart();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean increase(SkillValue val) {
		if (!canBeIncreased(val)) {
			logger.warn("Trying to increase attribute "+val+" which cannot be increased");
			return false;
		}

		val.setPoints(val.getPoints()+1);
		logger.info("Increase "+val.getModifyable()+" to "+val.getPoints());

		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean decrease(SkillValue val) {
		if (!canBeDecreased(val)) {
			logger.warn("Trying to increase attribute "+val+" which cannot be increased");
			return false;
		}

		val.setPoints(val.getPoints()-1);
		logger.info("Decrease "+val.getModifyable()+" to "+val.getPoints());

		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();
		toDos.clear();
//		conceptSkills.clear();
		try {
			logger.warn(" mods = "+previous);
			for (Modification tmp : previous) {
//				if (tmp instanceof KeySkillModification) {
//					KeySkillModification mod = (KeySkillModification)tmp;
//					conceptSkills.add(mod.getSkill());
//					logger.debug(" Add "+mod.getSkill()+" as concept skill");
//					System.err.println(" Add "+mod.getSkill()+" as concept skill");
//					continue;
//				} else 
					if (tmp instanceof SkillModification) {
					SkillModification mod = (SkillModification)tmp;
					logger.debug(" Apply "+mod);
					SkillValue aVal = parent.getCharacter().getSkillValue(mod.getSkill());
					aVal.addModification(mod);
					continue;
				}
				unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.SkillController#getPointsToSpend()
	 */
	@Override
	public int getPointsToSpend() {
		return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.SkillController#isConceptSkill(org.prelle.yearzeroengine.Skill)
	 */
	@Override
	public boolean isConceptSkill(Skill value) {
		return conceptSkills.contains(value);
	}

}
