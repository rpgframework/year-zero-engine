/**
 *
 */
package org.prelle.yearzeroengine.charlvl;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;
import org.prelle.yearzeroengine.charctrl.AttributeController;
import org.prelle.yearzeroengine.charctrl.CharacterClassController;
import org.prelle.yearzeroengine.charctrl.CharacterController;
import org.prelle.yearzeroengine.charctrl.FluffController;
import org.prelle.yearzeroengine.charctrl.GearController;
import org.prelle.yearzeroengine.charctrl.GroupController;
import org.prelle.yearzeroengine.charctrl.SkillController;
import org.prelle.yearzeroengine.charctrl.TalentController;
import org.prelle.yearzeroengine.chargen.YZECharGenConstants;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventType;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public abstract class CommonCharacterLeveller<T extends YearZeroEngineCharacter> implements CharacterController<T> {

	private final static Logger logger = YZECharGenConstants.LOGGER;

	protected T model;

	protected GroupController group;
	protected AttributeController attributes;
	protected CharacterClassController charclasses;
	protected SkillController skills;
	protected TalentController talents;
	protected GearController gear;
	protected FluffController fluff;

	protected List<CharacterProcessor> processChain;
//	protected Map<Modification, DecisionToMake> decisions;

	protected List<Modification> startModifications;

	//-------------------------------------------------------------------
	public CommonCharacterLeveller(YearZeroEngineRuleset ruleset, PropertyResourceBundle chargenI18n, T model) {
		this.model = model;
		attributes   = new NoOpAttributeLeveller<T>();
		skills       = new SkillLeveller<T>(this);
		talents      = new TalentLeveller<T>(this);
//		fluff        = new FluffGenerator<>(this);
//		charclasses  = new CharacterClassGenerator<T>(this, ruleset, chargenI18n);
		processChain = new ArrayList<>();
//		decisions    = new HashMap<>();
		startModifications = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getCharacter()
	 */
	@Override
	public T getCharacter() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getRuleset()
	 */
	@Override
	public YearZeroEngineRuleset getRuleset() {
		return model.getRuleset();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getGroupController()
	 */
	@Override
	public GroupController getGroupController() {
		return group;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return attributes;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		return skills;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getTalentController()
	 */
	@Override
	public TalentController getTalentController() {
		return talents;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getGearController()
	 */
	@Override
	public GearController getGearController() {
		return gear;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterController#getFluffController()
	 */
	@Override
	public FluffController getFluffController() {
		return fluff;
	}

	//-------------------------------------------------------------------
	public CharacterClassController getCharacterClassController() {
		return charclasses;
	}

	//-------------------------------------------------------------------
	public void addUnitTestModification(Modification mod) {
		startModifications.add(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	public void removeUnitTestModification(Modification mod) {
		startModifications.remove(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	public void runProcessors() {
		logger.debug("START: runProcessors: "+processChain.size()+"-------------------------------------------------------");
		try {
			List<Modification> unprocessed = new ArrayList<>(startModifications);
			logger.info("Start mods  = "+unprocessed);
			for (CharacterProcessor processor : processChain) {
				unprocessed = processor.process(model, unprocessed);
				logger.debug("------after "+processor.getClass().getSimpleName()+"     "+unprocessed);
//			/*
//			 * Check if any ModificationChoice has been added
//			 */
//			for (Modification mod : unprocessed) {
//				if (mod instanceof ModificationChoice) {
//					DecisionToMake decision = decisions.get(mod);
//					if (decision==null) {
//						// Yet unknown choice
//						logger.debug(" new decision to make: "+mod);
//						decision = new DecisionToMake(mod);
//						decisions.put(mod, decision);
//					}
//					unprocessed.remove(mod);
//				}
//			}

			}
			logger.info("Remaining mods  = "+unprocessed);
//		logger.info("Remaining karma = "+model.getKarmaFree());
		logger.info("ToDos = "+getToDos());
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, model));
		} finally {
			logger.info("STOP : runProcessors: "+processChain.size()+"-------------------------------------------------------");
		}
	}

	//-------------------------------------------------------------------
	public List<ToDoElement> getToDos() {
		List<ToDoElement> ret = new ArrayList<>();
		for (CharacterProcessor proc : processChain) {
			ret.addAll(proc.getToDos());
		}
		return ret;
	}

}
