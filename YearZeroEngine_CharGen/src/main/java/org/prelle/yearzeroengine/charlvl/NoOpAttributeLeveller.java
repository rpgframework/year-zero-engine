/**
 *
 */
package org.prelle.yearzeroengine.charlvl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.AttributeController;
import org.prelle.yearzeroengine.chargen.YZECharGenConstants;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;
import org.prelle.yearzeroengine.modifications.KeyAttributeModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class NoOpAttributeLeveller<T extends YearZeroEngineCharacter> implements AttributeController, CharacterProcessor {

	private final static Logger logger = YZECharGenConstants.LOGGER;

	protected List<ToDoElement> toDos;

	protected Attribute keyAttr;

	//--------------------------------------------------------------------
	public NoOpAttributeLeveller() {
		toDos = new ArrayList<>();

	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeIncreased(AttributeValue value) {
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeDecreased(AttributeValue value) {
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean increase(AttributeValue value) {
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean decrease(AttributeValue value) {
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.AttributeController#getPointsToSpend()
	 */
	@Override
	public int getPointsToSpend() {
		return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.AttributeController#getMaximum(org.prelle.yearzeroengine.Attribute)
	 */
	@Override
	public int getMaximum(Attribute key) {
		if (key==keyAttr)
			return 5;
		return 4;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model,
			List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();
		toDos.clear();
		try {
			for (Modification tmp : previous) {
				if (tmp instanceof KeyAttributeModification) {
					KeyAttributeModification mod = (KeyAttributeModification)tmp;
					keyAttr = mod.getAttribute();
					logger.debug(" Key attribute is "+keyAttr);
					continue;
				}
				unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

}
