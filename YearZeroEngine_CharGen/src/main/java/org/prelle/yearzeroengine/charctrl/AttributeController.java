/**
 *
 */
package org.prelle.yearzeroengine.charctrl;

import java.util.List;

import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.AttributeValue;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author prelle
 *
 */
public interface AttributeController extends NumericalValueController<Attribute,AttributeValue> {

	public int getPointsToSpend();

	public int getMaximum(Attribute key);

	public List<ToDoElement> getToDos();

}
