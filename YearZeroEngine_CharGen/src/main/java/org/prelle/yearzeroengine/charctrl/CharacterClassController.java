/**
 *
 */
package org.prelle.yearzeroengine.charctrl;

import java.util.List;

import org.prelle.yearzeroengine.CharacterClass;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;

import de.rpgframework.genericrpg.modification.DecisionToMake;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public interface CharacterClassController {

	public List<CharacterClass> getAvailableClasses();

	public CharacterClass getSelected();

	public void setSelected(CharacterClass value);

	public List<DecisionToMake> getDecisions();

	public void decide(DecisionToMake choice, Modification mod);

	//-------------------------------------------------------------------
	/**
	 * Create random relations to other player characters
	 */
	public void rollRandomRelations(YearZeroEngineCharacter model);

	//-------------------------------------------------------------------
	public void rollRandomFace(YearZeroEngineCharacter model);

	//-------------------------------------------------------------------
	public void rollRandomClothing(YearZeroEngineCharacter model);

}
