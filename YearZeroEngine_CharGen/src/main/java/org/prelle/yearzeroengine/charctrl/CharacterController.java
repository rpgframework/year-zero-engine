/**
 *
 */
package org.prelle.yearzeroengine.charctrl;

import java.util.List;

import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;

import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author prelle
 *
 */
public interface CharacterController<T extends YearZeroEngineCharacter> {

	//-------------------------------------------------------------------
	public T getCharacter();

	//-------------------------------------------------------------------
	public YearZeroEngineRuleset getRuleset();

	//-------------------------------------------------------------------
	public void runProcessors();

	//-------------------------------------------------------------------
	public List<ToDoElement> getToDos();

	//-------------------------------------------------------------------
	public GroupController getGroupController();

	//-------------------------------------------------------------------
	public AttributeController getAttributeController();

	//-------------------------------------------------------------------
	public SkillController getSkillController();

	//-------------------------------------------------------------------
	public TalentController getTalentController();

	//-------------------------------------------------------------------
	public GearController getGearController();

	//-------------------------------------------------------------------
	public FluffController getFluffController();

}
