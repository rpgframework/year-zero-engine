/**
 *
 */
package org.prelle.yearzeroengine.charctrl;

import java.util.List;

import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.TalentValue;

import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author prelle
 *
 */
public interface TalentController {

	public List<Talent> getAvailableTalents();

	public int getPointsToSpend();

	public boolean canBeSelected(Talent key);

	public boolean canBeDeselected(TalentValue key);

	public boolean select(Talent key);

	public boolean deselect(TalentValue key);

	public List<ToDoElement> getToDos();

}
