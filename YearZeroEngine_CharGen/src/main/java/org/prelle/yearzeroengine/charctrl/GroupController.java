/**
 * 
 */
package org.prelle.yearzeroengine.charctrl;

import java.util.Collection;
import java.util.List;

/**
 * @author prelle
 *
 */
public interface GroupController {

	public void setPCNames(Collection<String> names);
	
	public List<String> getPCNames();
	
}
