/**
 *
 */
package org.prelle.yearzeroengine.charctrl;

import java.util.List;

import org.prelle.yearzeroengine.CharacterRelation;

/**
 * @author Stefan
 *
 */
public interface FluffController {

	public void setName(String value);

	public void setPersonalProblem(String value);

	public void setFace(String value);

	public void setClothing(String value);


	public void addRelation(CharacterRelation value);

	public void removeRelation(CharacterRelation value);

	public List<CharacterRelation> getRelations();

}
