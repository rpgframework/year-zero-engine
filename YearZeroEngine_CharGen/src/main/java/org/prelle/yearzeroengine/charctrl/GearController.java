/**
 *
 */
package org.prelle.yearzeroengine.charctrl;

import java.util.List;

import org.prelle.yearzeroengine.CarriedItem;
import org.prelle.yearzeroengine.ItemLocation;
import org.prelle.yearzeroengine.ItemTemplate;

import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author Stefan
 *
 */
public interface GearController {

	public <T extends ItemTemplate> boolean canBeSelected(T item);

	public <T extends ItemTemplate> boolean canBeDeselected(CarriedItem<T> item);

	public <T extends ItemTemplate> CarriedItem<T> select(T value, ItemLocation loc);

	public <T extends ItemTemplate> void deselect(CarriedItem<T> value);

	public float getEncumbrancePointsLeft();

	public List<ToDoElement> getToDos();

}
