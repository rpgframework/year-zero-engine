/**
 *
 */
package org.prelle.yearzeroengine.charctrl;

import org.prelle.yearzeroengine.Skill;
import org.prelle.yearzeroengine.SkillValue;

import de.rpgframework.genericrpg.NumericalValueController;

/**
 * @author prelle
 *
 */
public interface SkillController extends NumericalValueController<Skill,SkillValue> {

	public int getPointsToSpend();

	public boolean isConceptSkill(Skill value);

}
