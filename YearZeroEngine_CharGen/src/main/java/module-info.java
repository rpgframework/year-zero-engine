/**
 * @author Stefan Prelle
 *
 */
module yearzeroengine.chargen {
	exports org.prelle.yearzeroengine.charctrl;
	exports org.prelle.yearzeroengine.chargen.event;
	exports org.prelle.yearzeroengine.chargen;
	exports org.prelle.yearzeroengine.charlvl;

	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive yearzeroengine.core;
}